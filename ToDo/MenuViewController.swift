//
//  MenuViewController.swift
//  ToDo
//
//  Created by Святослав Шевченко on 22.02.2021.
//

import UIKit

class MenuViewController: UITableViewController, GroupCellDelegate, GroupBigCellDelegate {
    
    func create() {
        let vc = storyboard?.instantiateViewController(withIdentifier: "GroupSettingsViewController") as! GroupSettingsViewController
        //vc.groupName = groups[cellIndex.row - 4]
        vc.vcID = 1
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func settings(cell: GroupCell) {
        let cellIndex = tableView.indexPath(for: cell)!
        let vc = storyboard?.instantiateViewController(withIdentifier: "GroupSettingsViewController") as! GroupSettingsViewController
        vc.groupName = groups[cellIndex.row - 4].name
        vc.groupID = String(groups[cellIndex.row - 4].id)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tasks(cell: GroupCell) {
        let cellIndex = tableView.indexPath(for: cell)!
        let vc = storyboard?.instantiateViewController(identifier: "TasksViewController") as? TasksViewController
        vc?.vcID = groups[cellIndex.row - 4].id
        vc?.groupName = groups[cellIndex.row - 4].name
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func finish(cell: GroupCell) {
        let cellIndex = tableView.indexPath(for: cell)!
        let vc = storyboard?.instantiateViewController(withIdentifier: "FinishViewController") as! FinishViewController
        vc.groupID = groups[cellIndex.row - 4].id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    var menu = ["Настройки", "Входящие задачи", "Исходящие задачи"]
    
    override func viewDidLoad() {
        print(UserDefaults.standard.string(forKey: "token"))
        var semaphore = DispatchSemaphore (value: 0)

        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token"),
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "http://todo.teo-crm.com/api/group/index")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            semaphore.signal()
            return
          }
            DispatchQueue.main.async { [self] () -> Void in
                do {
                    //print("i am here")
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] {
                        print(json)
                        for group in json  {
                            groups.append(Group(name: group["name"] as? String ?? "", link: group["link"] as? String ?? "", id: group["id"] as? Int ?? 0))
                        }
                        tableView.reloadData()
                        // try to read out a string array
                        //if let error = json["error"] as? String {
                        //    self.showAlertWithText(error)
                        //}
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
            semaphore.signal()
        }

        task.resume()
        semaphore.wait()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.separatorStyle = .none
        let taskCell = UINib(nibName: "GroupCell",
                                  bundle: nil)
        self.tableView.register(taskCell,
                                 forCellReuseIdentifier: "GroupCell")
        let groupCell = UINib(nibName: "GroupBigCell",
                                  bundle: nil)
        self.tableView.register(groupCell,
                                 forCellReuseIdentifier: "GroupBigCell")
    }
    
    struct Group {
        var name: String
        var link: String
        var id: Int
    }
    
    var groups = [Group]()
    
    var areGroupsShown = false
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if areGroupsShown {
            return 4 + groups.count
        } else {
            return 4
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (0...2).contains(indexPath.row) {
            let cell = UITableViewCell()
            cell.textLabel?.font = cell.textLabel?.font.withSize(21)
            cell.textLabel?.text = menu[indexPath.row]
            return cell
        } else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GroupBigCell") as! GroupBigCell
            cell.delegate = self
            if areGroupsShown {
                cell.plusLabel.text = "-"
            } else {
                cell.plusLabel.text = "+"
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GroupCell") as! GroupCell
            cell.delegate = self
            cell.groupNameLabel.text = groups[indexPath.row - 4].name
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (0...2).contains(indexPath.row) {
            return 60
        } else if indexPath.row == 3 {
            return 80
        } else {
            return 150
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 {
            let vc = storyboard?.instantiateViewController(identifier: "SettingsViewController") as! SettingsViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row == 1 {
            let vc = storyboard?.instantiateViewController(identifier: "TasksViewController") as? TasksViewController
            vc?.vcID = 0
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        if indexPath.row == 2 {
            let vc = storyboard?.instantiateViewController(identifier: "TasksViewController") as? TasksViewController
            vc?.vcID = 1
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        if indexPath.row == 3 {
            areGroupsShown.toggle()
            tableView.reloadData()
        }
    }
}
