//
//  TaskCell.swift
//  ToDo
//
//  Created by Святослав Шевченко on 17.02.2021.
//

import UIKit

class TaskCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descrLabel: UILabel!
    @IBOutlet weak var taskImage: UIImageView!
    @IBOutlet weak var profileImage: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        taskImage.layer.cornerRadius = 4
        profileImage.layer.cornerRadius = 47/2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
