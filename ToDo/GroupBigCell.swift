//
//  GroupBigCell.swift
//  ToDo
//
//  Created by Святослав Шевченко on 22.02.2021.
//

import UIKit

protocol GroupBigCellDelegate {
    func create()
}

class GroupBigCell: UITableViewCell {
    
    @IBOutlet weak var plusLabel: UILabel!
    
    var delegate: GroupBigCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func createTapped(_ sender: Any) {
        delegate?.create()
    }
}
