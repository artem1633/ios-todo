//
//  FinishSmallCell.swift
//  ToDo
//
//  Created by Святослав Шевченко on 23.02.2021.
//

import UIKit

class FinishSmallCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var summLabel: UILabel!
    @IBOutlet weak var outCountLabel: UILabel!
    @IBOutlet weak var noCountLabel: UILabel!
    @IBOutlet weak var yesCountLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
