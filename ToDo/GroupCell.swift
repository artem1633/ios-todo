//
//  GroupCell.swift
//  ToDo
//
//  Created by Святослав Шевченко on 22.02.2021.
//

import UIKit

protocol GroupCellDelegate {
    func settings(cell: GroupCell)
    func tasks(cell: GroupCell)
    func finish(cell: GroupCell)
}

class GroupCell: UITableViewCell {
    
    @IBOutlet weak var groupNameLabel: UILabel!
    
    var delegate: GroupCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func settingsTapped(_ sender: Any) {
        delegate?.settings(cell: self)
    }
    
    @IBAction func tasksTapped(_ sender: Any) {
        delegate?.tasks(cell: self)
    }
    
    @IBAction func finishTapped(_ sender: Any) {
        delegate?.finish(cell: self)
    }
    
}
