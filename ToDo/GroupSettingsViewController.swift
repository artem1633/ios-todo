//
//  GroupSettingsViewController.swift
//  ToDo
//
//  Created by Святослав Шевченко on 22.02.2021.
//

import UIKit
import Foundation

class GroupSettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ContactsViewControllerDelegate {
    
    var vcID = 0
    
    @IBOutlet weak var addImage: UIImageView!
    @IBOutlet weak var addLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var leaveButton: UIButton!
    
    
    
    func getContact(name: String, number: String) {
        members.append(Member(name: name, isAdmin: false, photo: "", id: 0))
        membersHeight.constant = CGFloat(70 * members.count)
        membersTable.reloadData()
        
        if vcID == 0 {
            let parameters = [
            ] as [[String : Any]]
            
            let boundary = "Boundary-\(UUID().uuidString)"
            var body = ""
            var error: Error? = nil
            for param in parameters {
                if param["disabled"] == nil {
                    let paramName = param["key"]!
                    body += "--\(boundary)\r\n"
                    body += "Content-Disposition:form-data; name=\"\(paramName)\""
                    if param["contentType"] != nil {
                        body += "\r\nContent-Type: \(param["contentType"] as! String)"
                    }
                    let paramType = param["type"] as! String
                    if paramType == "text" {
                        let paramValue = param["value"] as! String
                        body += "\r\n\r\n\(paramValue)\r\n"
                    } else {
                        let paramSrc = param["src"] as! String
                        let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                        let fileContent = String(data: fileData!, encoding: .utf8)!
                        body += "; filename=\"\(paramSrc)\"\r\n"
                            + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                    }
                }
            }
            body += "--\(boundary)--\r\n";
            let postData = body.data(using: .utf8)
            
            let token = UserDefaults.standard.string(forKey: "token")
            let urlString = "http://todo.teo-crm.com/api/group/add-user?phone=\(String(number.filter { !" ()-".contains($0) }))&id=\(groupID)&token=\(token!)"
            
            var request = URLRequest(url: URL(string: urlString)!,timeoutInterval: Double.infinity)
            request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            request.httpMethod = "GET"
            //request.httpBody = postData
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data else {
                    print(String(describing: error))
                    return
                }
                print(String(data: data, encoding: .utf8)!)
            }
            
            task.resume()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        if vcID == 0 {
            var semaphore = DispatchSemaphore (value: 0)
            
            let parameters = [
            ] as [[String : Any]]
            
            let boundary = "Boundary-\(UUID().uuidString)"
            var body = ""
            var error: Error? = nil
            for param in parameters {
                if param["disabled"] == nil {
                    let paramName = param["key"]!
                    body += "--\(boundary)\r\n"
                    body += "Content-Disposition:form-data; name=\"\(paramName)\""
                    if param["contentType"] != nil {
                        body += "\r\nContent-Type: \(param["contentType"] as! String)"
                    }
                    let paramType = param["type"] as! String
                    if paramType == "text" {
                        let paramValue = param["value"] as! String
                        body += "\r\n\r\n\(paramValue)\r\n"
                    } else {
                        let paramSrc = param["src"] as! String
                        let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                        let fileContent = String(data: fileData!, encoding: .utf8)!
                        body += "; filename=\"\(paramSrc)\"\r\n"
                            + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                    }
                }
            }
            body += "--\(boundary)--\r\n";
            let postData = body.data(using: .utf8)
            
            var token = UserDefaults.standard.string(forKey: "token")
            
            let urlString = "http://todo.teo-crm.com/api/group/view?token=\(token!)&id=\(groupID)"
            //print(urlString)
            
            var request = URLRequest(url: URL(string: urlString)!,timeoutInterval: Double.infinity)
            request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            request.httpMethod = "GET"
            //request.httpBody = postData
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data else {
                    print(String(describing: error))
                    semaphore.signal()
                    return
                }
                DispatchQueue.main.async { [self] () -> Void in
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                            print(json)
                            if let users = json["users"] as? [[String : Any]] {
                                members.removeAll()
                                for user in users {
                                    //let name =
                                    if user["fio"] as? String ?? "" == "" {
                                        members.append(Member(name: user["phone"] as? String ?? "", isAdmin: user["is_admin"] as? Bool ?? false, photo: user["avatar"] as? String ?? "", id: Int(user["id"] as? String ?? "0") ?? 0))
                                    } else {
                                        members.append(Member(name: user["fio"] as? String ?? "", isAdmin: user["is_admin"] as? Bool ?? false, photo: user["avatar"] as? String ?? "", id: Int(user["id"] as? String ?? "0") ?? 0))
                                    }
                                }
                                membersTable.reloadData()
                                membersHeight.constant = CGFloat(70 * members.count)
                            }
                            // try to read out a string array
                            //if let error = json["error"] as? String {
                            //    self.showAlertWithText(error)
                            //}
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                    }
                }
                semaphore.signal()
            }
            //print(members)
            task.resume()
            semaphore.wait()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return members.count
    }
    
    let model = DataModel.shared
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MemberCell") as! MemberCell
        cell.nameLabel.text = members[indexPath.row].name
        if !members[indexPath.row].isAdmin {
            cell.adminLabel.isHidden = true
        } else {
            if UserDefaults.standard.string(forKey: "id") == String(members[indexPath.row].id) {
                leaveButton.setTitle("Удалить группу", for: .normal)
                isAdmin = true
            }
        }
        DispatchQueue.main.async { [self] in
            if let url = URL(string: "http://todo.teo-crm.com/" + members[indexPath.row].photo){
                
                if let cachedImage = self.model.imageCache.object(forKey: url.absoluteString as NSString) {
                    cell.photoImage.image = cachedImage
                } else {
                    
                do {
                    let data = try Data(contentsOf: url)
                    cell.photoImage.image = UIImage(data: data)
                    self.model.imageCache.setObject((UIImage(data: data) ?? UIImage(named: "user"))!, forKey: url.absoluteString as NSString)
                } catch let err {
                    print("Error: \(err.localizedDescription)")
                }
                }
            } else {
                cell.photoImage.image = UIImage(named: "user")
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let minus = UIContextualAction(style: .destructive, title: "Удалить из группы") {[self] (action, view, success: (Bool) -> Void) in
            minusUser(index: indexPath.row)
            members.remove(at: indexPath.row)
            tableView.deleteRows(at:[indexPath], with: .fade)
            membersHeight.constant = CGFloat(members.count * 70)
            success(true)
        }
        let boss = UIContextualAction(style: .normal, title: "Назначить админом") {[self] (action, view, success: (Bool) -> Void) in
            success(true)
        }
        minus.backgroundColor = UIColor(red: 187/255, green: 62/255, blue: 28/255, alpha: 1)
        boss.backgroundColor = UIColor(red: 16/255, green: 90/255, blue: 201/255, alpha: 1)
        return UISwipeActionsConfiguration(actions: [minus, boss])
    }
    
    func minusUser(index: Int){
        print(members[index].id)
        let parameters = [
        ] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)
        
        let token = UserDefaults.standard.string(forKey: "token")
        
        let urlString = "http://todo.teo-crm.com/api/group/delete-user?token=\(token!)&id=\(groupID)&userId=\(members[index].id)"
        
        //print(urlString)

        var request = URLRequest(url: URL(string: urlString)!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "GET"
        //request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()
    }
    
    
    @IBOutlet weak var groupNameView: UIView!
    @IBOutlet weak var groupTF: UITextField!
    
    @IBOutlet weak var membersTable: UITableView!
    @IBOutlet weak var membersHeight: NSLayoutConstraint!
    
    struct Member {
        var name: String
        var isAdmin: Bool
        var photo: String
        var id: Int
    }
    
    var groupName = ""
    var groupID = ""
    
    var members = [Member]()
    
    @IBOutlet weak var addOrShareButton: UIButton!
    
    
    override func viewDidLoad() {
        print(groupID)
        super.viewDidLoad()
        groupNameView.layer.cornerRadius = 32
        groupTF.text = groupName
        membersTable.delegate = self
        membersTable.dataSource = self
        let taskCell = UINib(nibName: "MemberCell",
                                  bundle: nil)
        self.membersTable.register(taskCell,
                                 forCellReuseIdentifier: "MemberCell")
        membersHeight.constant = CGFloat(members.count * 70)
        if vcID == 1 {
            //addOrShareButton.setImage(UIImage(named: "add"), for: .normal)
            groupTF.text = "Название"
            groupTF.delegate = self
            addOrShareButton.setImage(nil, for: .normal)
            addOrShareButton.setTitle("+", for: .normal)
            addImage.isHidden = true
            addLabel.isHidden = true
            addButton.isHidden = true
            membersTable.isHidden = true
            leaveButton.isHidden = true
            
        } else {
            groupTF.delegate = self
            //groupTF.isEnabled = false
        }
    }
    
    @IBAction func addTapped(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "ContactsViewController") as! ContactsViewController
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func leaveTapped(_ sender: Any) {
        if isAdmin {
            let parameters = [
            ] as [[String : Any]]

            let boundary = "Boundary-\(UUID().uuidString)"
            var body = ""
            var error: Error? = nil
            for param in parameters {
              if param["disabled"] == nil {
                let paramName = param["key"]!
                body += "--\(boundary)\r\n"
                body += "Content-Disposition:form-data; name=\"\(paramName)\""
                if param["contentType"] != nil {
                  body += "\r\nContent-Type: \(param["contentType"] as! String)"
                }
                let paramType = param["type"] as! String
                if paramType == "text" {
                  let paramValue = param["value"] as! String
                  body += "\r\n\r\n\(paramValue)\r\n"
                } else {
                  let paramSrc = param["src"] as! String
                  let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                  let fileContent = String(data: fileData!, encoding: .utf8)!
                  body += "; filename=\"\(paramSrc)\"\r\n"
                    + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                }
              }
            }
            body += "--\(boundary)--\r\n";
            let postData = body.data(using: .utf8)
            
            let token = UserDefaults.standard.string(forKey: "token")
            let urlString = "http://todo.teo-crm.com/api/group/delete?token=\(token!)&id=\(groupID)"

            var request = URLRequest(url: URL(string: urlString)!,timeoutInterval: Double.infinity)
            request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

            request.httpMethod = "GET"
            //request.httpBody = postData

            let task = URLSession.shared.dataTask(with: request) { data, response, error in
              guard let data = data else {
                print(String(describing: error))
                return
              }
              print(String(data: data, encoding: .utf8)!)
            }

            task.resume()
        } else {
            let parameters = [
            ] as [[String : Any]]

            let boundary = "Boundary-\(UUID().uuidString)"
            var body = ""
            var error: Error? = nil
            for param in parameters {
              if param["disabled"] == nil {
                let paramName = param["key"]!
                body += "--\(boundary)\r\n"
                body += "Content-Disposition:form-data; name=\"\(paramName)\""
                if param["contentType"] != nil {
                  body += "\r\nContent-Type: \(param["contentType"] as! String)"
                }
                let paramType = param["type"] as! String
                if paramType == "text" {
                  let paramValue = param["value"] as! String
                  body += "\r\n\r\n\(paramValue)\r\n"
                } else {
                  let paramSrc = param["src"] as! String
                  let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                  let fileContent = String(data: fileData!, encoding: .utf8)!
                  body += "; filename=\"\(paramSrc)\"\r\n"
                    + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                }
              }
            }
            body += "--\(boundary)--\r\n";
            let postData = body.data(using: .utf8)
            
            let token = UserDefaults.standard.string(forKey: "token")
            let id = UserDefaults.standard.string(forKey: "id")
            
            let urlString = "http://todo.teo-crm.com/api/group/delete-user?token=\(token!)&id=\(groupID)&userId=\(id!)"
            
            //print(urlString)

            var request = URLRequest(url: URL(string: urlString)!,timeoutInterval: Double.infinity)
            request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

            request.httpMethod = "GET"
            //request.httpBody = postData

            let task = URLSession.shared.dataTask(with: request) { data, response, error in
              guard let data = data else {
                print(String(describing: error))
                return
              }
              print(String(data: data, encoding: .utf8)!)
            }

            task.resume()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backTapped(_ sender: Any) {
        if vcID == 0 {
            let parameters = [
              [
                "key": "token",
                "value": UserDefaults.standard.string(forKey: "token"),
                "type": "text"
              ],
              [
                "key": "name",
                "value": groupTF.text,
                "type": "text"
              ],
                [
                  "key": "id",
                  "value": groupID,
                  "type": "text"
                ]] as [[String : Any]]

            let boundary = "Boundary-\(UUID().uuidString)"
            var body = ""
            var error: Error? = nil
            for param in parameters {
              if param["disabled"] == nil {
                let paramName = param["key"]!
                body += "--\(boundary)\r\n"
                body += "Content-Disposition:form-data; name=\"\(paramName)\""
                if param["contentType"] != nil {
                  body += "\r\nContent-Type: \(param["contentType"] as! String)"
                }
                let paramType = param["type"] as! String
                if paramType == "text" {
                  let paramValue = param["value"] as! String
                  body += "\r\n\r\n\(paramValue)\r\n"
                } else {
                  let paramSrc = param["src"] as! String
                  let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                  let fileContent = String(data: fileData!, encoding: .utf8)!
                  body += "; filename=\"\(paramSrc)\"\r\n"
                    + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                }
              }
            }
            body += "--\(boundary)--\r\n";
            let postData = body.data(using: .utf8)

            var request = URLRequest(url: URL(string: "http://todo.teo-crm.com/api/group/update")!,timeoutInterval: Double.infinity)
            request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

            request.httpMethod = "POST"
            request.httpBody = postData

            let task = URLSession.shared.dataTask(with: request) { data, response, error in
              guard let data = data else {
                print(String(describing: error))
                return
              }
              print(String(data: data, encoding: .utf8)!)
            }

            task.resume()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shareTapped(_ sender: Any) {
        if vcID == 1 {
            let parameters = [
              [
                "key": "token",
                "value": UserDefaults.standard.string(forKey: "token"),
                "type": "text"
              ],
              [
                "key": "name",
                "value": groupTF.text,
                "type": "text"
              ]] as [[String : Any]]

            let boundary = "Boundary-\(UUID().uuidString)"
            var body = ""
            var error: Error? = nil
            for param in parameters {
              if param["disabled"] == nil {
                let paramName = param["key"]!
                body += "--\(boundary)\r\n"
                body += "Content-Disposition:form-data; name=\"\(paramName)\""
                if param["contentType"] != nil {
                  body += "\r\nContent-Type: \(param["contentType"] as! String)"
                }
                let paramType = param["type"] as! String
                if paramType == "text" {
                  let paramValue = param["value"] as! String
                  body += "\r\n\r\n\(paramValue)\r\n"
                } else {
                  let paramSrc = param["src"] as! String
                  let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                  let fileContent = String(data: fileData!, encoding: .utf8)!
                  body += "; filename=\"\(paramSrc)\"\r\n"
                    + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                }
              }
            }
            body += "--\(boundary)--\r\n";
            let postData = body.data(using: .utf8)

            var request = URLRequest(url: URL(string: "http://todo.teo-crm.com/api/group/create")!,timeoutInterval: Double.infinity)
            request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

            request.httpMethod = "POST"
            request.httpBody = postData

            let task = URLSession.shared.dataTask(with: request) { data, response, error in
              guard let data = data else {
                print(String(describing: error))
                return
              }
              print(String(data: data, encoding: .utf8)!)
            }

            task.resume()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    var isAdmin = false
}
