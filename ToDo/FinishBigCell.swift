//
//  FinishBigCell.swift
//  ToDo
//
//  Created by Святослав Шевченко on 23.02.2021.
//

import UIKit

class FinishBigCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {
    
    let model = DataModel.shared
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellTask = tableView.dequeueReusableCell(withIdentifier: "TaskCell") as! TaskCell
        cellTask.nameLabel.text = tasks[indexPath.row].name
        cellTask.descrLabel.text = tasks[indexPath.row].time + ", " + tasks[indexPath.row].summ + "₽"
        
        DispatchQueue.main.async { [self] in
            if let url = URL(string: "http://todo.teo-crm.com/\(tasks[indexPath.row].taskImage)"){
                //print("http://todo.teo-crm.com/\(totalTasks[indexPath.row].taskImage)")
                if let cachedImage = self.model.imageCache.object(forKey: url.absoluteString as NSString) {
                    cellTask.profileImage.image = cachedImage
                } else {
                    DispatchQueue.global(qos: .userInitiated).async {
                        let imageData = NSData.init(contentsOf: url)
                        DispatchQueue.main.async {
                            cellTask.profileImage.image = UIImage(data: imageData! as Data)
                            self.model.imageCache.setObject((UIImage(data: imageData! as Data) ?? UIImage(named: "user"))!, forKey: url.absoluteString as NSString)
                        }
                    }
                }
            } else {
                cellTask.profileImage.image = UIImage(named: "user")
            }
            if let url = URL(string: "http://todo.teo-crm.com/\(tasks[indexPath.row].profileImage)"){
                if let cachedImage = self.model.imageCache.object(forKey: url.absoluteString as NSString) {
                    cellTask.profileImage.image = cachedImage
                } else {
                    DispatchQueue.global(qos: .userInitiated).async {
                        let imageData = NSData.init(contentsOf: url)
                        DispatchQueue.main.async {
                            cellTask.taskImage.image = UIImage(data: imageData! as Data)
                            self.model.imageCache.setObject((UIImage(data: imageData! as Data) ?? UIImage(named: "user"))!, forKey: url.absoluteString as NSString)
                        }
                    }
                }
            }
        }
        
        
        return cellTask
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    var tasks = [TaskFinishBigCell]()
    
    @IBOutlet weak var nameFinishLabel: UILabel!
    @IBOutlet weak var summLabel: UILabel!
    @IBOutlet weak var outCountLabel: UILabel!
    @IBOutlet weak var noCountLabel: UILabel!
    @IBOutlet weak var yesCountLabel: UILabel!
    
    @IBOutlet weak var tasksTable: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tasksTable.delegate = self
        tasksTable.dataSource = self
        let taskCell = UINib(nibName: "TaskCell",
                                  bundle: nil)
        self.tasksTable.register(taskCell,
                                 forCellReuseIdentifier: "TaskCell")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

struct TaskFinishBigCell {
    var name: String
    var time: String
    var summ: String
    var profileImage: String
    var taskImage:  String
}
