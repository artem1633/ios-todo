//
//  ViewController.swift
//  ToDo
//
//  Created by Святослав Шевченко on 10.02.2021.
//

import UIKit

extension UIView {

    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
         let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
         let mask = CAShapeLayer()
         mask.path = path.cgPath
         self.layer.mask = mask
    }

}

extension UIViewController: UITextFieldDelegate {
    func showAlertWithText(_ text: String) {
        let alertWindow = UIAlertController(title: nil, message: text, preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "Закрыть", style: .cancel) { (UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }
        alertWindow.addAction(closeAction)
        self.present(alertWindow, animated: true, completion: nil)
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return false
    }
}

class RegViewController: UIViewController {
    
    @IBOutlet weak var regLabel: UILabel!
    @IBOutlet weak var tfVIew: UIView!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var numberTF: UITextField!
    @IBOutlet weak var tickImage: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    
    var nextOrigin : CGFloat = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        nextOrigin = self.nextButton.frame.origin.y
        tickImage.isHidden = true
        tfVIew.layer.cornerRadius = 8
        nextButton.layer.cornerRadius = 8
        tfVIew.layer.shadowColor = UIColor.black.cgColor
        tfVIew.layer.shadowOpacity = 0.3
        tfVIew.layer.shadowOffset = .zero
        tfVIew.layer.shadowRadius = 10
        numberTF.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        // Do any additional setup after loading the view.
    }

    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if isPhone {
            if (textField.text?.count == 11 && textField.text?.first == "8") || (textField.text?.count == 12 && textField.text?.first == "+") {
                tickImage.isHidden = false
            } else {
                tickImage.isHidden = true
            }
        } else {
            if textField.text?.count == 4 {
                tickImage.isHidden = false
            }
        }
    }
    
    var isPhone = true
    var isNext = false
    
    var phone = ""
    
    @IBAction func nextTapped(_ sender: Any) {
        if isNext {
            let vc = storyboard?.instantiateViewController(identifier: "TasksViewController") as? TasksViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        } else {
            if isPhone {
                if !tickImage.isHidden {
                    phone = numberTF.text ?? ""
                    var semaphore = DispatchSemaphore (value: 0)

                    let parameters = [
                      [
                        "key": "phone",
                        "value": numberTF.text,
                        "type": "text"
                      ]] as [[String : Any]]

                    let boundary = "Boundary-\(UUID().uuidString)"
                    var body = ""
                    var error: Error? = nil
                    for param in parameters {
                      if param["disabled"] == nil {
                        let paramName = param["key"]!
                        body += "--\(boundary)\r\n"
                        body += "Content-Disposition:form-data; name=\"\(paramName)\""
                        if param["contentType"] != nil {
                          body += "\r\nContent-Type: \(param["contentType"] as! String)"
                        }
                        let paramType = param["type"] as! String
                        if paramType == "text" {
                          let paramValue = param["value"] as! String
                          body += "\r\n\r\n\(paramValue)\r\n"
                        } else {
                          let paramSrc = param["src"] as! String
                          let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                          let fileContent = String(data: fileData!, encoding: .utf8)!
                          body += "; filename=\"\(paramSrc)\"\r\n"
                            + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                        }
                      }
                    }
                    body += "--\(boundary)--\r\n";
                    let postData = body.data(using: .utf8)

                    var request = URLRequest(url: URL(string: "http://todo.teo-crm.com/api/user/send-phone")!,timeoutInterval: Double.infinity)
                    request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

                    request.httpMethod = "POST"
                    request.httpBody = postData

                    let task = URLSession.shared.dataTask(with: request) { data, response, error in
                      guard let data = data else {
                        print(String(describing: error))
                        semaphore.signal()
                        return
                      }
                      semaphore.signal()
                    }

                    task.resume()
                    semaphore.wait()
                    regLabel.text = "Код из СМС"
                    numberLabel.text = "Введите код"
                    numberTF.text = ""
                    numberTF.isSecureTextEntry = true
                    isPhone = false
                    tickImage.isHidden = true
                }
            } else {
                if !tickImage.isHidden {
                    var semaphore = DispatchSemaphore (value: 0)

                    let parameters = [
                      [
                        "key": "phone",
                        "value": phone,
                        "type": "text"
                      ],
                      [
                        "key": "code",
                        "value": numberTF.text,
                        "type": "text"
                      ]] as [[String : Any]]

                    let boundary = "Boundary-\(UUID().uuidString)"
                    var body = ""
                    var error: Error? = nil
                    for param in parameters {
                      if param["disabled"] == nil {
                        let paramName = param["key"]!
                        body += "--\(boundary)\r\n"
                        body += "Content-Disposition:form-data; name=\"\(paramName)\""
                        if param["contentType"] != nil {
                          body += "\r\nContent-Type: \(param["contentType"] as! String)"
                        }
                        let paramType = param["type"] as! String
                        if paramType == "text" {
                          let paramValue = param["value"] as! String
                          body += "\r\n\r\n\(paramValue)\r\n"
                        } else {
                          let paramSrc = param["src"] as! String
                          let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                          let fileContent = String(data: fileData!, encoding: .utf8)!
                          body += "; filename=\"\(paramSrc)\"\r\n"
                            + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                        }
                      }
                    }
                    body += "--\(boundary)--\r\n";
                    let postData = body.data(using: .utf8)

                    var request = URLRequest(url: URL(string: "http://todo.teo-crm.com/api/user/get-token")!,timeoutInterval: Double.infinity)
                    request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

                    request.httpMethod = "POST"
                    request.httpBody = postData

                    let task = URLSession.shared.dataTask(with: request) { data, response, error in
                      guard let data = data else {
                        print(String(describing: error))
                        semaphore.signal()
                        return
                      }
                        DispatchQueue.main.async { () -> Void in
                            do {
                                //print("i am here")
                                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                                    print(json)
                                    if json["error"] == nil {
                                        
                                        UserDefaults.standard.set(json["token"] ,forKey: "token")
                                        self.view.endEditing(true)
                                        self.tfVIew.isHidden = true
                                        self.regLabel.text = "Благодарим\nза регистрацию!"
                                        self.isNext = true
                                        
                                    } else {
                                        self.showAlertWithText(json["error"] as? String ?? "")
                                    }
                                    // try to read out a string array
                                    //if let error = json["error"] as? String {
                                    //    self.showAlertWithText(error)
                                    //}
                                }
                            } catch let error as NSError {
                                print("Failed to load: \(error.localizedDescription)")
                            }
                        }
                        semaphore.signal()
                    }

                    task.resume()
                    semaphore.wait()
                    //view.endEditing(true)
                    //tfVIew.isHidden = true
                    //regLabel.text = "Благодарим\nза регистрацию!"
                    //isNext = true
                }
            }
        }
        
    }
    
}

