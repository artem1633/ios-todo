//
//  CalendarTasksViewController.swift
//  ToDo
//
//  Created by Святослав Шевченко on 04.03.2021.
//

import UIKit
import FSCalendar

struct Task {
    var id: String
    var taskImage: String
    var taskCreatorImage: String
    var name: String
    var time: String
    var summ: String
    var date: String
    var group_id: Int
    var user_id: Int
    var again: String
    var remember: String
}

class CalendarTasksViewController: UIViewController, FSCalendarDelegate, UITableViewDelegate, UITableViewDataSource, NewTaskViewControllerDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = storyboard?.instantiateViewController(identifier: "NewTaskViewController") as! NewTaskViewController
        vc.delegate = self
        vc.vcID = 1
        vc.idEdit = totalTasks[indexPath.row].id
        vc.nameEdit = totalTasks[indexPath.row].name
        vc.summEdit = totalTasks[indexPath.row].summ
        vc.certainTimeEdit = totalTasks[indexPath.row].time
        vc.dateEdit = totalTasks[indexPath.row].date
        vc.remindEdit = totalTasks[indexPath.row].remember
        vc.repeatEdit = totalTasks[indexPath.row].again
        vc.idMemberToSend = String(totalTasks[indexPath.row].user_id)
        vc.idGroupToSend = String(totalTasks[indexPath.row].group_id)
        if let url = URL(string: "http://todo.teo-crm.com/\(totalTasks[indexPath.row].taskImage)"){
            
            if let cachedImage = self.model.imageCache.object(forKey: url.absoluteString as NSString) {
                vc.imageEdit = cachedImage
            } else {
                do {
                    let data = try Data(contentsOf: url)
                    vc.imageEdit = UIImage(data: data)
                    self.model.imageCache.setObject((UIImage(data: data) ?? UIImage(named: "user"))!, forKey: url.absoluteString as NSString)
                } catch let err {
                    print("Error: \(err.localizedDescription)")
                }
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let minus = UIContextualAction(style: .destructive, title: nil) {[self] (action, view, success: (Bool) -> Void) in
            minusTaskToday(index: indexPath.row)
            totalTasks.remove(at: indexPath.row)
            tasksTable.reloadData()
            success(true)
        }
        minus.backgroundColor = UIColor(red: 187/255, green: 62/255, blue: 28/255, alpha: 1)
        minus.image = UIGraphicsImageRenderer(size: CGSize(width: 18, height: 20)).image { _ in
            UIImage(named: "bin")?.draw(in: CGRect(x: 0, y: 0, width: 18, height: 20))
        }
        return UISwipeActionsConfiguration(actions: [minus])
    }
    
    func minusTaskToday(index: Int){
        let parameters = [
        ] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)
        
        let token = UserDefaults.standard.string(forKey: "token")
        let urlString = "http://todo.teo-crm.com/api/task/delete?token=\(token!)&id=\(totalTasks[index].id)"

        var request = URLRequest(url: URL(string: urlString)!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "GET"
        //request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()

    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let plus = UIContextualAction(style: .normal, title: "Завершить") { [self] (action, view, success: (Bool) -> Void) in
            plusTaskToday(index: indexPath.row)
            totalTasks.remove(at: indexPath.row)
            tasksTable.reloadData()
            success(true)
        }
        plus.backgroundColor = UIColor(red: 0.067, green: 0.588, blue: 0.153, alpha: 1)
        return UISwipeActionsConfiguration(actions: [plus])
    }
    
    func plusTaskToday(index: Int){
        let parameters = [
        ] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)
        
        let token = UserDefaults.standard.string(forKey: "token")
        let urlString = "http://todo.teo-crm.com/api/task/complete?token=\(token!)&id=\(totalTasks[index].id)"

        var request = URLRequest(url: URL(string: urlString)!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "GET"
        //request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()

    }
    
    func getInfoFromNewTask() {
    }
    
    
    let model = DataModel.shared
    
    var totalTasks = [Task]()
    
    var vcID = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return totalTasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell") as! TaskCell
        cell.nameLabel.text = totalTasks[indexPath.row].name
        cell.descrLabel.text = "\(totalTasks[indexPath.row].time), \(totalTasks[indexPath.row].summ)₽"
        DispatchQueue.main.async { [self] in
            if let url = URL(string: "http://todo.teo-crm.com/\(totalTasks[indexPath.row].taskImage)"){
                //print("http://todo.teo-crm.com/\(totalTasks[indexPath.row].taskImage)")
                if let cachedImage = self.model.imageCache.object(forKey: url.absoluteString as NSString) {
                    cell.profileImage.image = cachedImage
                } else {
                    DispatchQueue.global(qos: .userInitiated).async {
                        let imageData = NSData.init(contentsOf: url)
                        DispatchQueue.main.async {
                            cell.profileImage.image = UIImage(data: imageData! as Data)
                            self.model.imageCache.setObject((UIImage(data: imageData! as Data) ?? UIImage(named: "user"))!, forKey: url.absoluteString as NSString)
                        }
                    }
                }
            } else {
                cell.profileImage.image = UIImage(named: "user")
            }
            if let url = URL(string: "http://todo.teo-crm.com/\(totalTasks[indexPath.row].taskCreatorImage)"){
                if let cachedImage = self.model.imageCache.object(forKey: url.absoluteString as NSString) {
                    cell.profileImage.image = cachedImage
                } else {
                    DispatchQueue.global(qos: .userInitiated).async {
                        let imageData = NSData.init(contentsOf: url)
                        DispatchQueue.main.async {
                            cell.taskImage.image = UIImage(data: imageData! as Data)
                            self.model.imageCache.setObject((UIImage(data: imageData! as Data) ?? UIImage(named: "user"))!, forKey: url.absoluteString as NSString)
                        }
                    }
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    @IBOutlet var calendar: FSCalendar!
    @IBOutlet weak var tasksTable: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        calendar.locale = Locale(identifier: "ru_RU") 
        calendar.delegate = self
        calendar.layer.cornerRadius = 20
        tasksTable.delegate = self
        tasksTable.dataSource = self
        let taskCell = UINib(nibName: "TaskCell",
                                  bundle: nil)
        self.tasksTable.register(taskCell,
                                 forCellReuseIdentifier: "TaskCell")
        tasksTable.tableFooterView = UIView(frame: .zero)
        calendar.layer.shadowRadius = 4
        calendar.layer.shadowColor = CGColor(red: 0, green: 0, blue: 0, alpha: 1)
        calendar.layer.shadowOpacity = 1
        calendar.layer.shadowOffset = .zero
        tasksTable.layer.shadowRadius = 4
        tasksTable.layer.shadowColor = CGColor(red: 0, green: 0, blue: 0, alpha: 1)
        tasksTable.layer.shadowOpacity = 1
        tasksTable.layer.shadowOffset = .zero
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        let string = formatter.string(from: date)
        selectedDate = string
        getInfoFromDate()
        print("selected \(string)")
    }
    
    var selectedDate = ""
    
    @IBAction func calendarTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func settingTapped(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "SettingsViewController") as! SettingsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    var groupName = ""
    
    func getInfoFromDate(){
        if vcID == 0 {
            var semaphore = DispatchSemaphore (value: 0)
            
            let parameters = [
                [
                    "key": "token",
                    "value": UserDefaults.standard.string(forKey: "token"),
                    "type": "text"
                ],
                [
                    "key": "date",
                    "value": selectedDate,
                    "type": "text"
                ]] as [[String : Any]]

            let boundary = "Boundary-\(UUID().uuidString)"
            var body = ""
            var error: Error? = nil
            for param in parameters {
              if param["disabled"] == nil {
                let paramName = param["key"]!
                body += "--\(boundary)\r\n"
                body += "Content-Disposition:form-data; name=\"\(paramName)\""
                if param["contentType"] != nil {
                  body += "\r\nContent-Type: \(param["contentType"] as! String)"
                }
                let paramType = param["type"] as! String
                if paramType == "text" {
                  let paramValue = param["value"] as! String
                  body += "\r\n\r\n\(paramValue)\r\n"
                } else {
                  let paramSrc = param["src"] as! String
                  let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                  let fileContent = String(data: fileData!, encoding: .utf8)!
                  body += "; filename=\"\(paramSrc)\"\r\n"
                    + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                }
              }
            }
            body += "--\(boundary)--\r\n";
            let postData = body.data(using: .utf8)

            var request = URLRequest(url: URL(string: "http://todo.teo-crm.com/api/task/index-in")!,timeoutInterval: Double.infinity)
            request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

            request.httpMethod = "POST"
            request.httpBody = postData

            let task = URLSession.shared.dataTask(with: request) { data, response, error in
              guard let data = data else {
                print(String(describing: error))
                semaphore.signal()
                return
              }
                DispatchQueue.main.async { [self] () -> Void in
                    do {
                        //print("i am here")
                        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] {
                            print(json)
                            totalTasks.removeAll()
                            for task in json  {
                                totalTasks.append(Task(id: String(task["id"] as? String ?? "0"), taskImage: task["avatar"] as? String ?? "", taskCreatorImage: task["photo_task"] as? String ?? "", name: task["name"] as? String ?? "", time: task["time"] as? String ?? "", summ: "\(task["price"] as? Int ?? 0)", date: "\(task["price"] as? Int ?? 0)", group_id: task["group_id"] as? Int ?? 0, user_id: task["user_id"] as? Int ?? 0, again: task["again"] as? String ?? "", remember: task["remember"] as? String ?? ""))
                            
                            }
                            print(totalTasks)
                                tasksTable.reloadData()

                            // try to read out a string array
                            //if let error = json["error"] as? String {
                            //    self.showAlertWithText(error)
                            //}
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                    }
                }
              semaphore.signal()
            }

            task.resume()
            //tasksTable.reloadData()
            semaphore.wait()
        } else if vcID == 1 {
            var semaphore = DispatchSemaphore (value: 0)

            let parameters = [
              [
                "key": "token",
                "value": UserDefaults.standard.string(forKey: "token"),
                "type": "text"
              ],
                [
                    "key": "date",
                    "value": selectedDate,
                    "type": "text"
                ]] as [[String : Any]]

            let boundary = "Boundary-\(UUID().uuidString)"
            var body = ""
            var error: Error? = nil
            for param in parameters {
              if param["disabled"] == nil {
                let paramName = param["key"]!
                body += "--\(boundary)\r\n"
                body += "Content-Disposition:form-data; name=\"\(paramName)\""
                if param["contentType"] != nil {
                  body += "\r\nContent-Type: \(param["contentType"] as! String)"
                }
                let paramType = param["type"] as! String
                if paramType == "text" {
                  let paramValue = param["value"] as! String
                  body += "\r\n\r\n\(paramValue)\r\n"
                } else {
                  let paramSrc = param["src"] as! String
                  let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                  let fileContent = String(data: fileData!, encoding: .utf8)!
                  body += "; filename=\"\(paramSrc)\"\r\n"
                    + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                }
              }
            }
            body += "--\(boundary)--\r\n";
            let postData = body.data(using: .utf8)

            var request = URLRequest(url: URL(string: "http://todo.teo-crm.com/api/task/index-out")!,timeoutInterval: Double.infinity)
            request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

            request.httpMethod = "POST"
            request.httpBody = postData

            let task = URLSession.shared.dataTask(with: request) { data, response, error in
              guard let data = data else {
                print(String(describing: error))
                semaphore.signal()
                return
              }
                DispatchQueue.main.async { [self] () -> Void in
                    do {
                        //print("i am here")
                        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] {
                            print(json)
                            totalTasks.removeAll()
                            for task in json  {
                                    totalTasks.append(Task(id: String(task["id"] as? String ?? "0"), taskImage: task["avatar"] as? String ?? "", taskCreatorImage: task["photo_task"] as? String ?? "", name: task["name"] as? String ?? "", time: task["time"] as? String ?? "", summ: "\(task["price"] as? Int ?? 0)", date: "\(task["price"] as? Int ?? 0)", group_id: task["group_id"] as? Int ?? 0, user_id: task["user_id"] as? Int ?? 0, again: task["again"] as? String ?? "", remember: task["remember"] as? String ?? ""))
                            }
                                tasksTable.reloadData()

                            // try to read out a string array
                            //if let error = json["error"] as? String {
                            //    self.showAlertWithText(error)
                            //}
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                    }
                }
              semaphore.signal()
            }

            task.resume()
            //tasksTable.reloadData()
            semaphore.wait()
        } else {
            var semaphore = DispatchSemaphore (value: 0)

            let parameters = [
            ] as [[String : Any]]

            let boundary = "Boundary-\(UUID().uuidString)"
            var body = ""
            var error: Error? = nil
            for param in parameters {
              if param["disabled"] == nil {
                let paramName = param["key"]!
                body += "--\(boundary)\r\n"
                body += "Content-Disposition:form-data; name=\"\(paramName)\""
                if param["contentType"] != nil {
                  body += "\r\nContent-Type: \(param["contentType"] as! String)"
                }
                let paramType = param["type"] as! String
                if paramType == "text" {
                  let paramValue = param["value"] as! String
                  body += "\r\n\r\n\(paramValue)\r\n"
                } else {
                  let paramSrc = param["src"] as! String
                  let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                  let fileContent = String(data: fileData!, encoding: .utf8)!
                  body += "; filename=\"\(paramSrc)\"\r\n"
                    + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                }
              }
            }
            body += "--\(boundary)--\r\n";
            let postData = body.data(using: .utf8)
            
            let token = UserDefaults.standard.string(forKey: "token")
            let day = selectedDate
            //let day
            let urlString = "http://todo.teo-crm.com/api/task/index-group?token=\(token!)&groupId=\(vcID)"
            //&date=\(day)"
            print(urlString)
            var request = URLRequest(url: URL(string: urlString)!,timeoutInterval: Double.infinity)
            request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

            request.httpMethod = "GET"
            //request.httpBody = postData

            let task = URLSession.shared.dataTask(with: request) { data, response, error in
              guard let data = data else {
                print(String(describing: error))
                semaphore.signal()
                return
              }
                DispatchQueue.main.async { [self] () -> Void in
                    do {
                        //print("i am here")
                        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] {
                            print(json)
                            totalTasks.removeAll()
                            for task in json  {
                                    totalTasks.append(Task(id: String(task["id"] as? String ?? "0"), taskImage: task["avatar"] as? String ?? "", taskCreatorImage: task["photo_task"] as? String ?? "", name: task["name"] as? String ?? "", time: task["time"] as? String ?? "", summ: "\(task["price"] as? Int ?? 0)", date: "\(task["price"] as? Int ?? 0)", group_id: task["group_id"] as? Int ?? 0, user_id: task["user_id"] as? Int ?? 0, again: task["again"] as? String ?? "", remember: task["remember"] as? String ?? ""))
                            }
                                tasksTable.reloadData()

                            // try to read out a string array
                            //if let error = json["error"] as? String {
                            //    self.showAlertWithText(error)
                            //}
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                    }
                }
              semaphore.signal()
            }

            task.resume()
            //tasksTable.reloadData()
            semaphore.wait()
        }
    }
    
    @IBAction func addTapped(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "NewTaskViewController") as! NewTaskViewController
        vc.delegate = self
        vc.groupID = vcID
        vc.dateEdit = selectedDate
        vc.groupName = groupName
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

