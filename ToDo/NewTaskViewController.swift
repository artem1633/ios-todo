//
//  NewTaskViewController.swift
//  ToDo
//
//  Created by Святослав Шевченко on 17.02.2021.
//

import UIKit

protocol NewTaskViewControllerDelegate {
    func getInfoFromNewTask()
}

class NewTaskViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    //MARK: - IMAGES
    
    @IBOutlet weak var fileImage: UIImageView!
    @IBOutlet weak var rubleLabel: UILabel!
    @IBOutlet weak var calendarImage: UIImageView!
    @IBOutlet weak var clockImage: UIImageView!
    @IBOutlet weak var bellImage: UIImageView!
    @IBOutlet weak var repeatImage: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var imageImage: UIImageView!
    @IBOutlet weak var imageLabel: UILabel!
    
    
    var delegate: NewTaskViewControllerDelegate?
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return 24
        } else {
            return 60
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if row < 10 {
            return "0\(row)"
        } else {
            return "\(row)"
        }
    }
    
    @IBOutlet weak var selectorView: UIView!
    @IBOutlet weak var selectorLabel: UILabel!
    
    
    var hours = "00"
    var minutes = "00"
    
    var isDatePicking = false
    
    var pickerTime = ""
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 {
            if row < 10 {
            hours = "0\(row)"
            } else {
                hours = "\(row)"
            }
        } else {
            if row < 10 {
                minutes = "0\(row)"
            } else {
                minutes = "\(row)"
            }
        }
        pickerTime = hours + ":" + minutes
        print("key key")
    }
    
    var cellHeight : CGFloat = 44
    
    var groupID = 0
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectors.count
    }
    
    var isMemberCell = false
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PickerTableCell") as! PickerTableCell
        cell.nameLabel.text = selectors[indexPath.row]
        if !isMemberCell {
            cell.selectorImage.image = selectorImages[indexPath.row]
            if cell.selectorImage.image == UIImage(named: "Vector-9") {
                cell.selectorImage.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
            }
        }
        cellHeight = cell.frame.height
        return cell
    }
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var pickerID = 0
    var selectorIndex = -1
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectorIndex = indexPath.row
//        if selectors == dateTasks {
//            if indexPath.row == 6 {
//                selectorView.isHidden = true
//                setDatePicker()
//                //self.present(datePicker, animated: true, completion: nil)
//            } else {
//                dateLabel.layer.borderWidth = 0
//                dateLabel.text = selectors[indexPath.row]
//                dateLabel.textColor = UIColor(red: 16/255, green: 90/255, blue: 201/255, alpha: 1)
//                selectorView.isHidden = true
//                tableView.isHidden  = true
//            }
//        } else if selectors == remindeTasks {
//            remindLabel.text = selectors[indexPath.row]
//            remindLabel.textColor = UIColor(red: 16/255, green: 90/255, blue: 201/255, alpha: 1)
//            selectorView.isHidden = true
//            tableView.isHidden = true
//        } else if selectors == repeatTasks {
//            repeatLabel.text = selectors[indexPath.row]
//            repeatLabel.textColor = UIColor(red: 16/255, green: 90/255, blue: 201/255, alpha: 1)
//            selectorView.isHidden = true
//            tableView.isHidden = true
//        } else if selectors == membersToAdd {
//            userTF.text = selectors[indexPath.row]
//            userTF.textColor = UIColor(red: 16/255, green: 90/255, blue: 201/255, alpha: 1)
//            idGroupToSend = membersToAddIds[indexPath.row].idGroup
//            idMemberToSend = membersToAddIds[indexPath.row].idMember
//            selectorView.isHidden = true
//            tableView.isHidden = true
//        }
    }
    
    var selectors = [String]()
    var selectorImages = [UIImage?]()
    
    var dateTasks = ["Сегодня", "Завтра", "Эта неделя", "След. неделя", "Этот месяц", "След. месяц", "Выбрать дату"]
    var remindeTasks = ["За 15 минут", "За час", "За 2 часа", "За день"]
    var repeatTasks = ["Ежедневно", "Рабочие дни", "Еженедельно", "Ежемесячно", "Ежегодно"]
    
    var dateImages = [UIImage(named: "calendar-1"), UIImage(named: "calendar-2"), UIImage(named: "calendar-3"), UIImage(named: "calendar-4"), UIImage(named: "calendar-5"), UIImage(named: "calendar-6"), UIImage(named: "date_range")]
    
    var remindeImages = [UIImage(named: "restore"), UIImage(named: "schedule"), UIImage(named: "arrow_circle_up"), UIImage(named: "date_range")]
    
    var repeatImages = [UIImage(named: "Vector-9"), UIImage(named: "drag_indicator"), UIImage(named: "drag_indicator-2"), UIImage(named: "drag_indicator-3"), UIImage(named: "drag_indicator-4")]
    
    
    @IBOutlet weak var newTaskLabel: UILabel!
    @IBOutlet weak var readyButton: UIButton!
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var summTF: UITextField!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var certainTimeLabel: UILabel!
    @IBOutlet weak var remindLabel: UILabel!
    @IBOutlet weak var repeatLabel: UILabel!
    @IBOutlet weak var userTF: UITextField!
    @IBOutlet weak var finishButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var deleteTimeButton: UIButton!
    @IBOutlet weak var deleteUserButton: UIButton!
    
    @IBOutlet weak var selectorTable: UITableView!
    
    @IBOutlet weak var tableHeightSelector: NSLayoutConstraint!
    
    @IBOutlet weak var tapToCloseView: UIView!
    
    
    
    var vcID = 0
    var groupName = ""
    
    @IBAction func timeTapped(_ sender: Any) {
        view.endEditing(true)
        //timePickerView.isHidden = false
        setTimePicker()
    }
    
    func setTimePicker(){
        pickerBigLabel.text = "Выбор времени"
        timePickerView.isHidden = false
        timePicker.isHidden = false
        datePicker.isHidden = true
    }
    
    func setDatePicker(){
        pickerBigLabel.text = "Выбор даты"
        timePickerView.isHidden = false
        timePicker.isHidden = true
        datePicker.isHidden = false
        //datePicker.datePickerMode = .date
        //let toolbar = UIToolbar();
        //toolbar.sizeToFit()
        //let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        //let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        //let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        //toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
    }
    
//    @objc func donedatePicker(){
//        datePicker.isHidden = true
//        timePickerView.isHidden = true
//        let formatter = DateFormatter()
//        formatter.dateFormat = "dd/MM/yyyy"
//        dateLabel.text = formatter.string(from: datePicker.date)
//        self.view.endEditing(true)
//    }
//
//    @objc func cancelDatePicker(){
//        datePicker.isHidden = true
//        timePickerView.isHidden = true
//    }
    
    @IBOutlet weak var pickerBigLabel: UILabel!
    
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        super.viewDidLoad()
        searchHeight.constant = 0
        selectorTable.backgroundColor = UIColor.systemBackground
        setView()
        let taskCell = UINib(nibName: "PickerTableCell",
                                  bundle: nil)
        self.selectorTable.register(taskCell,
                                 forCellReuseIdentifier: "PickerTableCell")
        datePicker.isHidden = true
        selectorTable.delegate = self
        selectorTable.dataSource = self
        if groupID == 0 {
            getGroups()
        } else {
            groups.append(Group(id: String(groupID), name: groupName, members: []))
            getMembers(groupId: String(groupID))
        }
        //deletePhotoButton.isHidden = true
        //addPhotoButton.isHidden = false
        deleteDate.isHidden = true
        deleteRemind.isHidden = true
        deleteRepeate.isHidden = true
        hideTableWhenTappedAround()
        nameTF.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        summTF.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        searchTF.addTarget(self, action: #selector(self.textFieldDidChange2(_:)), for: .editingChanged)
        searchTF.addTarget(self, action: #selector(self.textFieldDidBegin2(_:)), for: .editingDidBegin)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == nameTF {
            if textField.text != "" || textField.text != nil {
                fileImage.image = UIImage(named: "file")
            } else  {
                fileImage.image = UIImage(named: "file-2")
            }
        } else {
            if textField.text != "" || textField.text != nil {
                rubleLabel.textColor = UIColor(red: 16/255, green: 90/255, blue: 201/255, alpha: 1)
                deleteUserButton.isHidden = false
            } else {
                rubleLabel.textColor = UIColor.lightGray
            }
        }
    }
    
    func hideTableWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(hideKeyboard))
        tapToCloseView.addGestureRecognizer(tapGesture)
    }
    
    @objc func hideKeyboard() {
        print("key key")
        isMemberCell = false
        selectorView.isHidden = true
        selectorTable.isHidden  = true
        view.endEditing(true)
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    var groups = [Group]()
    
    struct Group {
        var id: String
        var name: String
        var members: [Member]
    }
    
    struct Member {
        var groupID: String
        var id: String
        var name: String
    }
    
    func getGroups(){
        var semaphore = DispatchSemaphore (value: 0)
        
        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token"),
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "http://todo.teo-crm.com/api/group/index")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            semaphore.signal()
            return
          }
            DispatchQueue.main.async { [self] () -> Void in
                do {
                    //print("i am here")
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] {
                        //print(json)
                        groups.removeAll()
                        for group in json  {
                            groups.append(Group(id: String(group["id"] as? Int ?? 0), name: group["name"] as? String ?? "", members: []))
                            getMembers(groupId: String(group["id"] as? Int ?? 0))
                        }
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
            semaphore.signal()
        }
        
        task.resume()
        semaphore.wait()
    }
    
    var members = [Member]()
    
    func getMembers(groupId: String){
        var semaphore = DispatchSemaphore (value: 0)
        
        let parameters = [
        ] as [[String : Any]]
        
        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
            if param["disabled"] == nil {
                let paramName = param["key"]!
                body += "--\(boundary)\r\n"
                body += "Content-Disposition:form-data; name=\"\(paramName)\""
                if param["contentType"] != nil {
                    body += "\r\nContent-Type: \(param["contentType"] as! String)"
                }
                let paramType = param["type"] as! String
                if paramType == "text" {
                    let paramValue = param["value"] as! String
                    body += "\r\n\r\n\(paramValue)\r\n"
                } else {
                    let paramSrc = param["src"] as! String
                    let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                    let fileContent = String(data: fileData!, encoding: .utf8)!
                    body += "; filename=\"\(paramSrc)\"\r\n"
                        + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                }
            }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)
        
        var token = UserDefaults.standard.string(forKey: "token")
        
        let urlString = "http://todo.teo-crm.com/api/group/view?token=\(token!)&id=\(groupId)"
        //print(urlString)
        
        var request = URLRequest(url: URL(string: urlString)!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        request.httpMethod = "GET"
        //request.httpBody = postData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                semaphore.signal()
                return
            }
            DispatchQueue.main.async { [self] () -> Void in
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        //print(json)
                        if let users = json["users"] as? [[String : Any]] {
                            //members.removeAll()
                            for user in users {
                                if user["fio"] as? String ?? "" == "" {
                                    members.append(Member(groupID: groupId, id: user["id"] as? String ?? "OPOP", name: user["phone"] as? String ?? ""))
                                } else {
                                    members.append(Member(groupID: groupId, id: user["id"] as? String ?? "OPOP", name: user["fio"] as? String ?? ""))
                                }
                                //print(members)
                            }
                        }
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
            semaphore.signal()
        }
        //print(members)
        task.resume()
        semaphore.wait()
    }
    
    
    
    //MARK:- EDIT
    
    var idEdit = ""
    var nameEdit = ""
    var summEdit = ""
    var dateEdit = ""
    var certainTimeEdit = ""
    var remindEdit = ""
    var repeatEdit = ""
    var userEdit = ""
    var imageEdit: UIImage?
    
    @IBOutlet weak var miniPickerView: UIView!
    @IBOutlet weak var miniSelectorView: UIView!
    
    
    func setView(){
        miniSelectorView.layer.cornerRadius = 15
        miniPickerView.layer.cornerRadius = 15
//        miniSelectorView.roundCorners([.topLeft,  .topRight], radius: 15)
//        miniPickerView.roundCorners([.topLeft,  .topRight], radius: 15)
        //selectorTable.backgroundColor = UIColor.black
        timePickerView.isHidden = true
        timePicker.delegate = self
        timePicker.dataSource = self
        timeView.isHidden = true
        timePicker.layer.cornerRadius = 20
        selectorView.isHidden = true
        selectorTable.isHidden = true
        selectorTable.tableFooterView = UIView(frame: .zero)
        if vcID == 0 {
            readyButton.layer.cornerRadius = 8
            timeView.layer.cornerRadius = 6
            finishButton.isHidden = true
            deleteButton.isHidden = true
            deleteTimeButton.isHidden = true
            deleteUserButton.isHidden = true
            deletePhotoButton.isHidden = true
        } else if vcID == 1 {
            newTaskLabel.text = "Редактирование"
            //addPhotoButton.isHidden = true
            //deletePhotoButton.isHidden = false
            finishButton.isHidden = true
            deleteButton.isHidden = true
            deleteTimeButton.isHidden = true
            deleteUserButton.isHidden = true
            readyButton.layer.cornerRadius = 8
            timeView.layer.cornerRadius = 6
            nameTF.text = nameEdit
            summTF.text = summEdit
            dateLabel.text = dateEdit
            certainTimeLabel.text = certainTimeEdit
            remindLabel.text = remindEdit
            repeatLabel.text = repeatEdit
            userTF.text = userEdit
            if imageEdit != nil {
                addPhotoButton.isHidden = true
                photoTask.image = imageEdit
                deletePhotoButton.isHidden = false
            }
        } else if vcID == 2 {
            
        }
    }
    
    @IBOutlet weak var timePickerView: UIView!
    @IBOutlet weak var timePicker: UIPickerView!
    
    
    @IBAction func backTimeTapped(_ sender: Any) {
        timePickerView.isHidden = true
    }
    
    @IBAction func doneTimeTapped(_ sender: Any) {
        if timePicker.isHidden {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            dateLabel.text = formatter.string(from: datePicker.date)
            dateLabel.textColor = UIColor(red: 16/255, green: 90/255, blue: 201/255, alpha: 1)
            calendarImage.image = UIImage(named: "calendar")
            timePickerView.isHidden = true
            //deleteDate.isHidden = false
        } else {
            timeLabel.textColor = UIColor(red: 16/255, green: 90/255, blue: 201/255, alpha: 1)
            timeView.isHidden = true
            timeLabel.text = pickerTime
            timePickerView.isHidden = true
            deleteTimeButton.isHidden = false
            clockImage.image = UIImage(named: "clock")
        }
        
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func readyTapped(_ sender: Any) {
        if vcID == 0 {
            if nameTF.text != "" && summTF.text != "" && userTF.text != "" && dateLabel.text != "Дата задачи" {
                sendReadyRequest()
                delegate?.getInfoFromNewTask()
                self.navigationController?.popViewController(animated: true)
                
            } else {
                if nameTF.text == "" {
                    nameTF.layer.borderWidth = 1
                    nameTF.layer.borderColor = CGColor(red: 1, green: 0, blue: 0, alpha: 1)
                }
                if summTF.text == "" {
                    summTF.layer.borderWidth = 1
                    summTF.layer.borderColor = CGColor(red: 1, green: 0, blue: 0, alpha: 1)
                }
                if userTF.text == "" {
                    userTF.layer.borderWidth = 1
                    userTF.layer.borderColor = CGColor(red: 1, green: 0, blue: 0, alpha: 1)
                }
                if dateLabel.text == "Дата задачи" {
                    dateLabel.layer.borderWidth = 1
                    dateLabel.layer.borderColor = CGColor(red: 1, green: 0, blue: 0, alpha: 1)
                }
            }
        } else {
            if nameTF.text != "" && summTF.text != "" && userTF.text != "" && dateLabel.text != "Дата задачи" {
                sendEditRequest()
                self.navigationController?.popViewController(animated: true)
            } else {
                if nameTF.text == "" {
                    nameTF.layer.borderWidth = 1
                    nameTF.layer.borderColor = CGColor(red: 1, green: 0, blue: 0, alpha: 1)
                }
                if summTF.text == "" {
                    summTF.layer.borderWidth = 1
                    summTF.layer.borderColor = CGColor(red: 1, green: 0, blue: 0, alpha: 1)
                }
                if userTF.text == "" {
                    userTF.layer.borderWidth = 1
                    userTF.layer.borderColor = CGColor(red: 1, green: 0, blue: 0, alpha: 1)
                }
                if dateLabel.text == "Дата задачи" {
                    dateLabel.layer.borderWidth = 1
                    dateLabel.layer.borderColor = CGColor(red: 1, green: 0, blue: 0, alpha: 1)
                }
            }
        }

    }
    
    @IBAction func dateTapped(_ sender: Any) {
        view.endEditing(true)
        tableHeightSelector.constant = CGFloat(dateTasks.count) * cellHeight
        selectorLabel.text = "Выбор даты"
        selectorView.isHidden = false
        selectorTable.isHidden = false
        selectorImages = dateImages
        selectors = dateTasks
        selectorTable.reloadData()
    }
    
    @IBAction func remindTapped(_ sender: Any) {
        view.endEditing(true)
        tableHeightSelector.constant = CGFloat(remindeTasks.count) * cellHeight
        selectorLabel.text = "Напомнить"
        selectorView.isHidden = false
        selectorTable.isHidden = false
        selectorImages = remindeImages
        selectors = remindeTasks
        selectorTable.reloadData()
    }
    
    @IBAction func repeatTapped(_ sender: Any) {
        view.endEditing(true)
        selectorLabel.text = "Повтор"
        tableHeightSelector.constant = CGFloat(repeatTasks.count) * cellHeight
        selectorView.isHidden = false
        selectorTable.isHidden = false
        selectorImages = repeatImages
        selectors = repeatTasks
        selectorTable.reloadData()
    }
    
    @IBAction func closeSelectorView(_ sender: Any) {
        isMemberCell = false
        searchHeight.constant = 0
        selectorView.isHidden = true
        view.endEditing(true)
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    
    var membersToAdd = [String]()
    
    var membersToAddIds = [(idMember: String, idGroup: String)]()
    var membersToAddIds2 = [(idMember: String, idGroup: String)]()
    
    var idMemberToSend = ""
    var idGroupToSend = ""
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var membersToAdd2 = [String]()
    
    @IBAction func memberTapped(_ sender: Any) {
        isMemberCell = true
        searchHeight.constant = 40
        view.endEditing(true)
        membersToAdd.removeAll()
        for group in groups {
            for member in members {
                if member.groupID == group.id {
                    membersToAdd.append("Имя: " + member.name + ", Группа: " + group.name)
                    membersToAddIds.append((idMember: member.id, idGroup: member.groupID))
                }
            }
        }
        membersToAddIds2 = membersToAddIds
        membersToAdd2 = membersToAdd
        selectorLabel.text = "Назначить"
        tableHeightSelector.constant = CGFloat(membersToAdd.count) * cellHeight
        
        searchController.searchResultsUpdater = self
        // 2
        searchController.obscuresBackgroundDuringPresentation = false
        // 3
        searchController.searchBar.placeholder = "Поиск"
        // 4
        navigationItem.searchController = searchController
        // 5
        definesPresentationContext = true

        selectorView.isHidden = false
        selectorTable.isHidden = false
        selectors = membersToAdd
        selectorTable.reloadData()
    }
    
    var isFiltering: Bool {
      return searchController.isActive && !isSearchBarEmpty
    }
    
    var isSearchBarEmpty: Bool {
      return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filterMembersToAdd = membersToAdd.filter { (name: String) -> Bool in
        return name.lowercased().contains(searchText.lowercased())
      }
      
        selectorTable.reloadData()
    }
    
    var filterMembersToAdd = [String]()
    
    var imagePicker = UIImagePickerController()
    
    @IBAction func addPhotoTapped(_ sender: Any) {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let albumAction = UIAlertAction(title: "Альбом", style: .default) { [self] (UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                
                imagePicker.delegate = self
                imagePicker.sourceType = .photoLibrary
                imagePicker.allowsEditing = false
                
                present(imagePicker, animated: true, completion: nil)
            }
        }
        let cameraAction = UIAlertAction(title: "Камера", style: .default) { [self] (UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                
                imagePicker.delegate = self
                imagePicker.sourceType = .camera
                imagePicker.allowsEditing = false
                
                present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(albumAction)
        present(actionSheet, animated: true, completion: nil)
    }
    
    @IBOutlet weak var photoTask: UIImageView!
    @IBOutlet weak var addPhotoButton: UIButton!
    @IBOutlet weak var deletePhotoButton: UIButton!
    
    
    @IBAction func deletePhotoTapped(_ sender: Any) {
        photoTask.image = nil
        imageImage.image = UIImage(named: "image-2")
        imageLabel.textColor = UIColor.lightGray
        deletePhotoButton.isHidden = true
        addPhotoButton.isHidden = false
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        deletePhotoButton.isHidden = false
        addPhotoButton.isHidden = true
        photoTask.image = image
        imageImage.image = UIImage(named: "image")
        imageLabel.textColor = UIColor(red: 16/255, green: 90/255, blue: 201/255, alpha: 1)
    }
    
    func sendEditRequest(){
        if photoTask.image != nil {
            imageUploadRequest(imageView: photoTask.image!, uploadUrl: URL(string: "http://todo.teo-crm.com/api/task/update")! as NSURL, param: ["id" : idEdit,"token" : UserDefaults.standard.string(forKey: "token")!, "name" : nameTF.text!, "executor_id" : idMemberToSend, "group_id" : idGroupToSend, "price" : summTF.text!, "date" : dateLabel.text!, "time" : timeLabel.text!, "remember" : remindLabel.text!, "again" : repeatLabel.text!, "coords_start" : ""])
        }
    }
    
    func sendReadyRequest(){
        if photoTask.image != nil {
            imageUploadRequest(imageView: photoTask.image!, uploadUrl: URL(string: "http://todo.teo-crm.com/api/task/create")! as NSURL, param: ["token" : UserDefaults.standard.string(forKey: "token")!, "name" : nameTF.text!, "executor_id" : idMemberToSend, "group_id" : idGroupToSend, "price" : summTF.text!, "date" : dateLabel.text!, "time" : timeLabel.text!, "remember" : remindLabel.text!, "again" : repeatLabel.text!, "coords_start" : ""])
        }
    }
    
    func imageUploadRequest(imageView: UIImage, uploadUrl: NSURL, param: [String:String]?) {

            //let myUrl = NSURL(string: "http://192.168.1.103/upload.photo/index.php");

        let request = NSMutableURLRequest(url:uploadUrl as URL);
        request.httpMethod = "POST"

            let boundary = generateBoundaryString()

            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        let imageData = imageView.jpegData(compressionQuality: 1)

            if(imageData==nil)  { return; }

        request.httpBody = createBodyWithParameters(parameters: param, filePathKey: "photo_task", imageDataKey: imageData! as NSData, boundary: boundary) as Data

            //myActivityIndicator.startAnimating();

        let task =  URLSession.shared.dataTask(with: request as URLRequest,
                completionHandler: {
                    (data, response, error) -> Void in
                    if let data = data {

                        // You can print out response object
                        print("******* response = \(response)")

                        print(data.count)
                        // you can use data here

                        // Print out reponse body
                        let responseString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                        print("****** response data = \(responseString!)")

                        let json =  try!JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary

                        print("json value \(json)")

                        //var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers, error: &err)

                    } else if let error = error {
                        print(error)
                    }
            })
            task.resume()


        }


        func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
            let body = NSMutableData();

            if parameters != nil {
                for (key, value) in parameters! {
                    body.appendString(string: "--\(boundary)\r\n")
                    body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                    body.appendString(string: "\(value)\r\n")
                }
            }

            let filename = "user-profile.jpg"

            let mimetype = "image/jpg"

            body.appendString(string: "--\(boundary)\r\n")
            body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
            body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
            body.append(imageDataKey as Data)
            body.appendString(string: "\r\n")

            body.appendString(string: "--\(boundary)--\r\n")

            return body
        }

        func generateBoundaryString() -> String {
            return "Boundary-\(NSUUID().uuidString)"
        }
    
    @IBAction func readySelectorTapped(_ sender: Any) {
        if selectorIndex == -1 {
            selectorView.isHidden = true
            selectorTable.isHidden  = true
        } else {
            if selectors == dateTasks {
                if selectorIndex == 6 {
                    selectorView.isHidden = true
                    setDatePicker()
                    //self.present(datePicker, animated: true, completion: nil)
                } else {
                    dateLabel.layer.borderWidth = 0
                    dateLabel.text = selectors[selectorIndex]
                    dateLabel.textColor = UIColor(red: 16/255, green: 90/255, blue: 201/255, alpha: 1)
                    selectorView.isHidden = true
                    selectorTable.isHidden  = true
                    //deleteDate.isHidden = false
                    calendarImage.image = UIImage(named: "calendar")
                }
            } else if selectors == remindeTasks {
                remindLabel.text = selectors[selectorIndex]
                remindLabel.textColor = UIColor(red: 16/255, green: 90/255, blue: 201/255, alpha: 1)
                deleteRemind.isHidden = false
                bellImage.image = UIImage(named: "bell")
                selectorView.isHidden = true
                selectorTable.isHidden = true
                
            } else if selectors == repeatTasks {
                repeatLabel.text = selectors[selectorIndex]
                repeatLabel.textColor = UIColor(red: 16/255, green: 90/255, blue: 201/255, alpha: 1)
                repeatImage.image = UIImage(named: "repeat")
                deleteRepeate.isHidden = false
                selectorView.isHidden = true
                selectorTable.isHidden = true
            } else if selectors == membersToAdd {
                isMemberCell = false
                searchHeight.constant = 0
                userTF.text = selectors[selectorIndex]
                userTF.textColor = UIColor(red: 16/255, green: 90/255, blue: 201/255, alpha: 1)
                userImage.image = UIImage(named: "user")
                idGroupToSend = membersToAddIds[selectorIndex].idGroup
                idMemberToSend = membersToAddIds[selectorIndex].idMember
                //deleteUserButton.isHidden = false
                selectorView.isHidden = true
                selectorTable.isHidden = true
                
            }
        }
        selectorIndex = -1
        view.endEditing(true)
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @IBOutlet weak var deleteDate: UIButton!
    @IBOutlet weak var deleteRemind: UIButton!
    @IBOutlet weak var deleteRepeate: UIButton!
    
    //MARK:- DELETE LINES
    
    @IBAction func deleteRemind(_ sender: Any) {
        remindLabel.text = "Напомнить"
        remindLabel.textColor = UIColor.lightGray
        deleteRemind.isHidden = true
        bellImage.image = UIImage(named: "bell-2")
    }
    
    @IBAction func deleteRepeat(_ sender: Any) {
        repeatImage.image = UIImage(named: "repeat-2")
        repeatLabel.text = "Повтор"
        repeatLabel.textColor = UIColor.lightGray
        deleteRepeate.isHidden = true
    }
    
    @IBAction func deleteSumm(_ sender: Any) {
        summTF.text = nil
        deleteUserButton.isHidden = true
        rubleLabel.textColor = UIColor.lightGray
    }
    
    @IBAction func deleteTime(_ sender: Any) {
        timeLabel.textColor = UIColor.lightGray
        timeLabel.text = "Время задачи"
        deleteTimeButton.isHidden = true
        clockImage.image = UIImage(named: "clock-2")
    }
    
    //MARK:- SEARCH
    
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchHeight: NSLayoutConstraint!
    
    var sortedMembers = [String]()
    var sortedMembersID = [(idMember: String, idGroup: String)]()
    
    @objc func textFieldDidChange2(_ textField: UITextField) {
//        print(textField.text)
        if textField.text != "" {
            sortedMembers.removeAll()
            sortedMembersID.removeAll()
            for indexMember in 0..<membersToAdd2.count {
                if membersToAdd2[indexMember].contains(textField.text ?? "") {
                    sortedMembers.append(membersToAdd2[indexMember])
                    sortedMembersID.append(membersToAddIds2[indexMember])
                }
            }
            membersToAdd = sortedMembers
            selectors = membersToAdd
            membersToAddIds = sortedMembersID
        } else {
            
            membersToAdd = membersToAdd2
            membersToAdd = membersToAdd2
            selectors = membersToAdd
            print(selectors)
        }
        print("3")
        selectorTable.reloadData()
        
    }
    
    @objc func textFieldDidBegin2(_ textField: UITextField) {
        print("opopop")
        if self.view.frame.origin.y == 0 {
            self.view.frame.origin.y -= keyboardSizeGlobal
        }
    }
    
    var keyboardSizeGlobal : CGFloat = 260
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            keyboardSizeGlobal = keyboardSize.height
            print(keyboardSizeGlobal)
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
    }
}

extension NewTaskViewController: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    let searchBar = searchController.searchBar
    filterContentForSearchText(searchBar.text!)
  }
}
