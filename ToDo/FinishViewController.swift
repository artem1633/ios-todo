//
//  FinishViewController.swift
//  ToDo
//
//  Created by Святослав Шевченко on 23.02.2021.
//

import UIKit

class FinishViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource {
    
    var groupID = 0
    
    struct Finish {
        var name: String
        var summ: String
        var yesCount: String
        var noCount: String
        var outCount: String
        var tasks: [TaskFinishBigCell]
    }
    
    var finishes = [Finish(name: "Юрий Мизулин", summ: "13423", yesCount: "10", noCount: "10", outCount: "10", tasks: [TaskFinishBigCell(name: "opopop", time: "23:20", summ: "228", profileImage: "", taskImage: ""), TaskFinishBigCell(name: "opopop", time: "23:20", summ: "228", profileImage: "", taskImage: ""), TaskFinishBigCell(name: "opopop", time: "23:20", summ: "228", profileImage: "", taskImage: "")]), Finish(name: "Юрий Мизулин", summ: "13423", yesCount: "10", noCount: "10", outCount: "10", tasks: [TaskFinishBigCell(name: "opopop", time: "23:20", summ: "228", profileImage: "", taskImage: "")]), Finish(name: "Юрий Мизулин", summ: "13423", yesCount: "10", noCount: "10", outCount: "10", tasks: [TaskFinishBigCell(name: "opopop", time: "23:20", summ: "228", profileImage: "", taskImage: "")]), Finish(name: "Юрий Мизулин", summ: "13423", yesCount: "10", noCount: "10", outCount: "10", tasks: [TaskFinishBigCell(name: "opopop", time: "23:20", summ: "228", profileImage: "", taskImage: "")])]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return finishes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if dayColor == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FinishSmallCell") as! FinishSmallCell
            cell.nameLabel.text = finishes[indexPath.row].name
            cell.summLabel.text = finishes[indexPath.row].summ + " ₽"
            cell.outCountLabel.text = finishes[indexPath.row].outCount
            cell.yesCountLabel.text = finishes[indexPath.row].yesCount
            cell.noCountLabel.text = finishes[indexPath.row].noCount
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FinishBigCell") as! FinishBigCell
            cell.nameFinishLabel.text = finishes[indexPath.row].name
            cell.summLabel.text = finishes[indexPath.row].summ + " ₽"
            cell.outCountLabel.text = finishes[indexPath.row].outCount
            cell.yesCountLabel.text = finishes[indexPath.row].yesCount
            cell.noCountLabel.text = finishes[indexPath.row].noCount
            cell.tasks = finishes[indexPath.row].tasks
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if dayColor == 0 {
            return 80
        } else {
            return CGFloat(125 + finishes[indexPath.row].tasks.count * 80)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return days.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DaysCells", for: indexPath) as! DaysCells
        cell.dayLabel.text = days[indexPath.item]
        
        if indexPath.item == dayColor {
            cell.dayLabel.textColor = UIColor(red: 16/255, green: 90/255, blue: 201/255, alpha: 1)
        } else {
            cell.dayLabel.textColor = UIColor(red: 78/255, green: 78/255, blue: 78/255, alpha: 1)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        dayColor = indexPath.item
        collectionView.deselectItem(at: indexPath, animated: true)
        collectionView.reloadData()
        finishsTable.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.item == 0 {
            return CGSize(width: 170, height: 35)
        } else {
            return CGSize(width: 210, height: 35)
        }
    }
    
    
    @IBOutlet weak var typeCollection: UICollectionView!
    @IBOutlet weak var finishsTable: UITableView!
    
    let days = ["Свернутый", "Расширенный"]
    var dayColor = 0
    
    override func viewWillAppear(_ animated: Bool) {
        var semaphore = DispatchSemaphore (value: 0)

        let parameters = [
        ] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)
        
        let token = UserDefaults.standard.string(forKey: "token")
        let urlString = "http://todo.teo-crm.com/api/user/report?token=\(token!)&id=\(groupID)"

        var request = URLRequest(url: URL(string: urlString)!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "GET"
        //request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            semaphore.signal()
            return
          }
            DispatchQueue.main.async { [self] () -> Void in
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] {
                        //print(json)
                        finishes.removeAll()
                        for finish in json {
                            var tasksFinishes = [TaskFinishBigCell]()
                            if let tasks = finish["tasks"] as? [[String : Any]] {
                                for task in tasks {
                                    
                                }
                            }
                            finishes.append(Finish(name: finish["fio"] as? String ?? "FIO", summ: finish["tasksCompletedSum"] as? String ?? "0", yesCount: finish["tasksCompleted"] as? String ?? "", noCount: finish["tasksNotCompleted"] as? String ?? "", outCount: finish["tasksExcept"] as? String ?? "", tasks: tasksFinishes))
                        }
                        finishsTable.reloadData()
                        // try to read out a string array
                        //if let error = json["error"] as? String {
                        //    self.showAlertWithText(error)
                        //}
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
            semaphore.signal()
        }

        task.resume()
        semaphore.wait()
    }
    
    override func viewDidLoad() {
        print(groupID)
        super.viewDidLoad()
        typeCollection.delegate = self
        typeCollection.dataSource = self
        let productCell = UINib(nibName: "DaysCells",
                                  bundle: nil)
        self.typeCollection.register(productCell,
                                     forCellWithReuseIdentifier: "DaysCells")
        finishsTable.delegate = self
        finishsTable.dataSource = self
        finishsTable.tableFooterView = UIView(frame: .zero)
        let finishSmall = UINib(nibName: "FinishSmallCell",
                                  bundle: nil)
        self.finishsTable.register(finishSmall,
                                 forCellReuseIdentifier: "FinishSmallCell")
        let finishBig = UINib(nibName: "FinishBigCell",
                                  bundle: nil)
        self.finishsTable.register(finishBig,
                                 forCellReuseIdentifier: "FinishBigCell")
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shareTapped(_ sender: Any) {
    }
    
    @IBAction func settingsTapped(_ sender: Any) {
    }
    
}
