//
//  ContactsViewController.swift
//  ToDo
//
//  Created by Святослав Шевченко on 23.02.2021.
//

import UIKit
import Contacts

protocol ContactsViewControllerDelegate {
    func getContact(name: String, number: String)
}

struct Contact {
    var givenName: String
    var lastName: String
    var number: String
}

class ContactsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var delegate: ContactsViewControllerDelegate?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        cell.textLabel?.text = contacts[indexPath.row].givenName + " " + contacts[indexPath.row].lastName
        cell.detailTextLabel?.text = contacts[indexPath.row].number
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        delegate?.getContact(name: contacts[indexPath.row].givenName + " " + contacts[indexPath.row].lastName, number: contacts[indexPath.row].number)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBOutlet weak var contactsTable: UITableView!
    
    var contactStore = CNContactStore()
    var contacts = [Contact]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contactsTable.delegate = self
        contactsTable.dataSource = self
        contactStore.requestAccess(for: .contacts) { (success, error) in
            if success {
                print("success")
            }
        }
        contactsTable.tableFooterView = UIView(frame: .zero)
        fetchContacts()
    }
    
    func fetchContacts() {
        
        let key = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey] as [CNKeyDescriptor]
        let request = CNContactFetchRequest(keysToFetch: key)
        try! contactStore.enumerateContacts(with: request) { [self] (contact, stoppingPointer) in
            let name = contact.givenName
            let familyName = contact.familyName
            let number = contact.phoneNumbers.first?.value.stringValue
            
            let contactToAppend = Contact(givenName: name, lastName: familyName, number: number ?? "0")
            
            contacts.append(contactToAppend)
        }
        contactsTable.reloadData()
        print(contacts.first?.givenName)
    }
    
}
