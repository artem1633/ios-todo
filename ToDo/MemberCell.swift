//
//  MemberCell.swift
//  ToDo
//
//  Created by Святослав Шевченко on 22.02.2021.
//

import UIKit

class MemberCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var adminLabel: UILabel!
    @IBOutlet weak var photoImage: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        photoImage.layer.cornerRadius = 27
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
