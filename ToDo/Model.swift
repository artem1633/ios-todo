//
//  Model.swift
//  ToDo
//
//  Created by Святослав Шевченко on 17.02.2021.
//

import Foundation
import UIKit

class DataModel {
    
    static let shared = DataModel()
    
    var imageCache = NSCache<NSString, UIImage>()
}

