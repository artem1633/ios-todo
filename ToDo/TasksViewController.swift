//
//  TasksViewController.swift
//  ToDo
//
//  Created by Святослав Шевченко on 17.02.2021.
//

import UIKit

class FirstNav: UINavigationController {
    
}

class TasksViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource, NewTaskViewControllerDelegate {
    
    func getInfoFromNewTask() {
        getInfo()
        tasksTable.reloadData()
        print(totalTasks)
    }
    
    
    let model = DataModel.shared
    
    var vcID = 0
    
    static let shared = TasksViewController()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return totalTasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell") as! TaskCell
        cell.nameLabel.text = totalTasks[indexPath.row].name
        //DispatchQueue.main.async { [self] in
        cell.descrLabel.text = "\(totalTasks[indexPath.row].time), \(totalTasks[indexPath.row].summ)₽"
        DispatchQueue.global(qos: .userInitiated).async { [self] in
        if let url = URL(string: "http://todo.teo-crm.com/\(totalTasks[indexPath.row].taskImage)"){
            //print("http://todo.teo-crm.com/\(totalTasks[indexPath.row].taskImage)")
            if let cachedImage = self.model.imageCache.object(forKey: url.absoluteString as NSString) {
                DispatchQueue.main.async {
                    cell.profileImage.image = cachedImage
                }
                
            } else {
                DispatchQueue.global(qos: .userInitiated).async {
                    let imageData = NSData.init(contentsOf: url)
                    DispatchQueue.main.async {
                        if url == URL(string: "http://todo.teo-crm.com/\(totalTasks[indexPath.row].taskImage)"){
                            cell.profileImage.image = UIImage(data: imageData! as Data)
                            self.model.imageCache.setObject((UIImage(data: imageData! as Data) ?? UIImage(named: "user"))!, forKey: url.absoluteString as NSString)
                        }
                    }
                }
//                do {
//                    let data = try Data(contentsOf: url)
//                    cell.profileImage.image = UIImage(data: data)
//                    self.model.imageCache.setObject((UIImage(data: data) ?? UIImage(named: "user"))!, forKey: url.absoluteString as NSString)
//                } catch let err {
//                    print("Error: \(err.localizedDescription)")
//                }
            }
        } else {
            cell.profileImage.image = UIImage(named: "user")
        }
        if let url = URL(string: "http://todo.teo-crm.com/\(totalTasks[indexPath.row].taskCreatorImage)"){
            //print("http://todo.teo-crm.com/\(totalTasks[indexPath.row].taskImage)")
            if let cachedImage = self.model.imageCache.object(forKey: url.absoluteString as NSString) {
                //print("OPOPOP")
                DispatchQueue.main.async {
                    cell.profileImage.image = cachedImage
                }
                
            } else {
                DispatchQueue.global(qos: .userInitiated).async {
                    let imageData = NSData.init(contentsOf: url)
                    DispatchQueue.main.async {
                        if url == URL(string: "http://todo.teo-crm.com/\(totalTasks[indexPath.row].taskCreatorImage)") {
                            cell.taskImage.image = UIImage(data: imageData! as Data)
                            self.model.imageCache.setObject((UIImage(data: imageData! as Data) ?? UIImage(named: "user"))!, forKey: url.absoluteString as NSString)
                        }
                    }
                }
//                do {
//                    let data = try Data(contentsOf: url)
//                    cell.taskImage.image = UIImage(data: data)
//                    self.model.imageCache.setObject((UIImage(data: data) ?? UIImage(named: "user"))!, forKey: url.absoluteString as NSString)
//                } catch let err {
//                    print("Error: \(err.localizedDescription)")
//                }
            }
        }
        }
   //     }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let minus = UIContextualAction(style: .destructive, title: nil) {[self] (action, view, success: (Bool) -> Void) in
            if dayColor == 0 {
                minusTaskToday(index: indexPath.row)
                todayTasks.remove(at: indexPath.row)
                totalTasks = todayTasks
            }
            if dayColor == 1 {
                minusTaskNext(index: indexPath.row)
                nextTasks.remove(at: indexPath.row)
                totalTasks = nextTasks
            }
            if dayColor == 2 {
                minusTaskWeek(index: indexPath.row)
                weekTasks.remove(at: indexPath.row)
                totalTasks = weekTasks
            }
            tasksTable.reloadData()
            success(true)
        }
        minus.backgroundColor = UIColor(red: 187/255, green: 62/255, blue: 28/255, alpha: 1)
        minus.image = UIGraphicsImageRenderer(size: CGSize(width: 18, height: 20)).image { _ in
            UIImage(named: "bin")?.draw(in: CGRect(x: 0, y: 0, width: 18, height: 20))
        }
        return UISwipeActionsConfiguration(actions: [minus])
    }
    
    func minusTaskToday(index: Int){
        let parameters = [
        ] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)
        
        let token = UserDefaults.standard.string(forKey: "token")
        let urlString = "http://todo.teo-crm.com/api/task/delete?token=\(token!)&id=\(todayTasks[index].id)"

        var request = URLRequest(url: URL(string: urlString)!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "GET"
        //request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()

    }
    
    func minusTaskNext(index: Int){
        let parameters = [
        ] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)
        
        let token = UserDefaults.standard.string(forKey: "token")
        let urlString = "http://todo.teo-crm.com/api/task/delete?token=\(token!)&id=\(nextTasks[index].id)"

        var request = URLRequest(url: URL(string: urlString)!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "GET"
        //request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()

    }
    
    func minusTaskWeek(index: Int){
        let parameters = [
        ] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)
        
        let token = UserDefaults.standard.string(forKey: "token")
        let urlString = "http://todo.teo-crm.com/api/task/delete?token=\(token!)&id=\(weekTasks[index].id)"

        var request = URLRequest(url: URL(string: urlString)!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "GET"
        //request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let plus = UIContextualAction(style: .normal, title: "Завершить") { [self] (action, view, success: (Bool) -> Void) in
            if dayColor == 0 {
                plusTaskToday(index: indexPath.row)
                todayTasks.remove(at: indexPath.row)
                totalTasks = todayTasks
            }
            if dayColor == 1 {
                plusTaskNext(index: indexPath.row)
                nextTasks.remove(at: indexPath.row)
                totalTasks = nextTasks
            }
            if dayColor == 2 {
                plusTaskWeek(index: indexPath.row)
                weekTasks.remove(at: indexPath.row)
                totalTasks = weekTasks
            }
            tasksTable.reloadData()
            success(true)
        }
        plus.backgroundColor = UIColor(red: 0.067, green: 0.588, blue: 0.153, alpha: 1)
        return UISwipeActionsConfiguration(actions: [plus])
    }
    
    func plusTaskToday(index: Int){
        let parameters = [
        ] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)
        
        let token = UserDefaults.standard.string(forKey: "token")
        let urlString = "http://todo.teo-crm.com/api/task/complete?token=\(token!)&id=\(todayTasks[index].id)"

        var request = URLRequest(url: URL(string: urlString)!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "GET"
        //request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()

    }
    
    func plusTaskNext(index: Int){
        let parameters = [
        ] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)
        
        let token = UserDefaults.standard.string(forKey: "token")
        let urlString = "http://todo.teo-crm.com/api/task/complete?token=\(token!)&id=\(nextTasks[index].id)"

        var request = URLRequest(url: URL(string: urlString)!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "GET"
        //request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()

    }
    
    func plusTaskWeek(index: Int){
        let parameters = [
        ] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)
        
        let token = UserDefaults.standard.string(forKey: "token")
        let urlString = "http://todo.teo-crm.com/api/task/complete?token=\(token!)&id=\(weekTasks[index].id)"

        var request = URLRequest(url: URL(string: urlString)!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "GET"
        //request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return days.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DaysCells", for: indexPath) as! DaysCells
        cell.dayLabel.text = days[indexPath.item]
        
        if indexPath.item == dayColor {
            cell.dayLabel.textColor = UIColor(red: 16/255, green: 90/255, blue: 201/255, alpha: 1)
        } else {
            cell.dayLabel.textColor = UIColor(red: 78/255, green: 78/255, blue: 78/255, alpha: 1)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        dayColor = indexPath.item
        if indexPath.item == 0 {
            getInfo()
            //totalTasks = todayTasks
        }
        if indexPath.item == 1 {
            getInfo()
            //totalTasks = nextTasks
        }
        if indexPath.item == 2 {
            getInfo()
            //totalTasks = weekTasks
        }
        if totalTasks.count == 0 {
            tasksTable.isHidden = true
            noTasksLabel.isHidden = false
        } else {
            tasksTable.isHidden = false
            noTasksLabel.isHidden = true
        }
        collectionView.deselectItem(at: indexPath, animated: true)
        collectionView.reloadData()
        tasksTable.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.item == 0 {
            return CGSize(width: 130, height: 35)
        }
        else if indexPath.item == 1 {
            return CGSize(width: 110, height: 35)
        }
        else if indexPath.item == 2 {
            return CGSize(width: 170, height: 35)
        }
        else {
            return CGSize(width: 150, height: 35)
        }
    }
    
    
    @IBOutlet weak var daysCollection: UICollectionView!
    @IBOutlet weak var tasksTable: UITableView!
    @IBOutlet weak var noTasksLabel: UILabel!
    
    
    
    let days = ["Сегодня", "Завтра", "Эта неделя"]
    var dayColor = 0
    
    var todayTasks = [Task]()
    var nextTasks = [Task]()
    var weekTasks = [Task]()
    
    var totalTasks = [Task]()
    
    
    override func viewWillAppear(_ animated: Bool) {
        getInfo()
        tasksTable.reloadData()
    }
    
    func getInfo(){
        if vcID == 0 {
            var semaphore = DispatchSemaphore (value: 0)
            
            let parameters = [
                [
                    "key": "token",
                    "value": UserDefaults.standard.string(forKey: "token"),
                    "type": "text"
                ],
                [
                    "key": "date",
                    "value": days[dayColor],
                    "type": "text"
                ]] as [[String : Any]]

            let boundary = "Boundary-\(UUID().uuidString)"
            var body = ""
            var error: Error? = nil
            for param in parameters {
              if param["disabled"] == nil {
                let paramName = param["key"]!
                body += "--\(boundary)\r\n"
                body += "Content-Disposition:form-data; name=\"\(paramName)\""
                if param["contentType"] != nil {
                  body += "\r\nContent-Type: \(param["contentType"] as! String)"
                }
                let paramType = param["type"] as! String
                if paramType == "text" {
                  let paramValue = param["value"] as! String
                  body += "\r\n\r\n\(paramValue)\r\n"
                } else {
                  let paramSrc = param["src"] as! String
                  let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                  let fileContent = String(data: fileData!, encoding: .utf8)!
                  body += "; filename=\"\(paramSrc)\"\r\n"
                    + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                }
              }
            }
            body += "--\(boundary)--\r\n";
            let postData = body.data(using: .utf8)

            var request = URLRequest(url: URL(string: "http://todo.teo-crm.com/api/task/index-in")!,timeoutInterval: Double.infinity)
            request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

            request.httpMethod = "POST"
            request.httpBody = postData

            let task = URLSession.shared.dataTask(with: request) { data, response, error in
              guard let data = data else {
                print(String(describing: error))
                semaphore.signal()
                return
              }
                DispatchQueue.main.async { [self] () -> Void in
                    do {
                        //print("i am here")
                        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] {
                            print(json)
                            todayTasks.removeAll()
                            nextTasks.removeAll()
                            weekTasks.removeAll()
                            for task in json  {
                                if dayColor == 0 {
                                todayTasks.append(Task(id: String(task["id"] as? String ?? "0"), taskImage: task["avatar"] as? String ?? "", taskCreatorImage: task["photo_task"] as? String ?? "", name: task["name"] as? String ?? "", time: task["time"] as? String ?? "", summ: "\(task["price"] as? Int ?? 0)", date: "\(task["price"] as? Int ?? 0)", group_id: task["group_id"] as? Int ?? 0, user_id: task["user_id"] as? Int ?? 0, again: task["again"] as? String ?? "", remember: task["remember"] as? String ?? ""))
                                }
                                if dayColor == 1 {
                                nextTasks.append(Task(id: String(task["id"] as? String ?? "0"), taskImage: task["avatar"] as? String ?? "", taskCreatorImage: task["photo_task"] as? String ?? "", name: task["name"] as? String ?? "", time: task["time"] as? String ?? "", summ: "\(task["price"] as? Int ?? 0)", date: "\(task["price"] as? Int ?? 0)", group_id: task["group_id"] as? Int ?? 0, user_id: task["user_id"] as? Int ?? 0, again: task["again"] as? String ?? "", remember: task["remember"] as? String ?? ""))
                                }
                                if dayColor == 2 {
                                weekTasks.append(Task(id: String(task["id"] as? String ?? "0"), taskImage: task["avatar"] as? String ?? "", taskCreatorImage: task["photo_task"] as? String ?? "", name: task["name"] as? String ?? "", time: task["time"] as? String ?? "", summ: "\(task["price"] as? Int ?? 0)", date: "\(task["price"] as? Int ?? 0)", group_id: task["group_id"] as? Int ?? 0, user_id: task["user_id"] as? Int ?? 0, again: task["again"] as? String ?? "", remember: task["remember"] as? String ?? ""))
                                }
                            }
                            if dayColor == 0 {
                                if todayTasks.count == 0 {
                                    print("empty")
                                    tasksTable.isHidden = true
                                } else {
                                    totalTasks = todayTasks
                                    noTasksLabel.isHidden = true
                                    tasksTable.reloadData()
                                }
                                tasksTable.reloadData()
                            }
                            if dayColor == 1 {
                                if nextTasks.count == 0 {
                                    print("empty")
                                    tasksTable.isHidden = true
                                } else {
                                    totalTasks = nextTasks
                                    noTasksLabel.isHidden = true
                                    tasksTable.reloadData()
                                }
                                tasksTable.reloadData()
                            }
                            if dayColor == 2 {
                                if weekTasks.count == 0 {
                                    print("empty")
                                    tasksTable.isHidden = true
                                } else {
                                    totalTasks = weekTasks
                                    noTasksLabel.isHidden = true
                                    tasksTable.reloadData()
                                }
                                tasksTable.reloadData()
                            }

                            // try to read out a string array
                            //if let error = json["error"] as? String {
                            //    self.showAlertWithText(error)
                            //}
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                    }
                }
              semaphore.signal()
            }
            tasksTable.reloadData()
            task.resume()
            //tasksTable.reloadData()
            semaphore.wait()
        } else if vcID == 1 {
            var semaphore = DispatchSemaphore (value: 0)

            let parameters = [
              [
                "key": "token",
                "value": UserDefaults.standard.string(forKey: "token"),
                "type": "text"
              ],
                [
                    "key": "date",
                    "value": days[dayColor],
                    "type": "text"
                ]] as [[String : Any]]

            let boundary = "Boundary-\(UUID().uuidString)"
            var body = ""
            var error: Error? = nil
            for param in parameters {
              if param["disabled"] == nil {
                let paramName = param["key"]!
                body += "--\(boundary)\r\n"
                body += "Content-Disposition:form-data; name=\"\(paramName)\""
                if param["contentType"] != nil {
                  body += "\r\nContent-Type: \(param["contentType"] as! String)"
                }
                let paramType = param["type"] as! String
                if paramType == "text" {
                  let paramValue = param["value"] as! String
                  body += "\r\n\r\n\(paramValue)\r\n"
                } else {
                  let paramSrc = param["src"] as! String
                  let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                  let fileContent = String(data: fileData!, encoding: .utf8)!
                  body += "; filename=\"\(paramSrc)\"\r\n"
                    + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                }
              }
            }
            body += "--\(boundary)--\r\n";
            let postData = body.data(using: .utf8)

            var request = URLRequest(url: URL(string: "http://todo.teo-crm.com/api/task/index-out")!,timeoutInterval: Double.infinity)
            request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

            request.httpMethod = "POST"
            request.httpBody = postData

            let task = URLSession.shared.dataTask(with: request) { data, response, error in
              guard let data = data else {
                print(String(describing: error))
                semaphore.signal()
                return
              }
                DispatchQueue.main.async { [self] () -> Void in
                    do {
                        //print("i am here")
                        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] {
                            print(json)
                            todayTasks.removeAll()
                            nextTasks.removeAll()
                            weekTasks.removeAll()
                            for task in json  {
                                if dayColor == 0 {
                                todayTasks.append(Task(id: String(task["id"] as? String ?? "0"), taskImage: task["avatar"] as? String ?? "", taskCreatorImage: task["photo_task"] as? String ?? "", name: task["name"] as? String ?? "", time: task["time"] as? String ?? "", summ: "\(task["price"] as? Int ?? 0)", date: "\(task["price"] as? Int ?? 0)", group_id: task["group_id"] as? Int ?? 0, user_id: task["user_id"] as? Int ?? 0, again: task["again"] as? String ?? "", remember: task["remember"] as? String ?? ""))
                                }
                                if dayColor == 1 {
                                nextTasks.append(Task(id: String(task["id"] as? String ?? "0"), taskImage: task["avatar"] as? String ?? "", taskCreatorImage: task["photo_task"] as? String ?? "", name: task["name"] as? String ?? "", time: task["time"] as? String ?? "", summ: "\(task["price"] as? Int ?? 0)", date: "\(task["price"] as? Int ?? 0)", group_id: task["group_id"] as? Int ?? 0, user_id: task["user_id"] as? Int ?? 0, again: task["again"] as? String ?? "", remember: task["remember"] as? String ?? ""))
                                }
                                if dayColor == 2 {
                                weekTasks.append(Task(id: String(task["id"] as? String ?? "0"), taskImage: task["avatar"] as? String ?? "", taskCreatorImage: task["photo_task"] as? String ?? "", name: task["name"] as? String ?? "", time: task["time"] as? String ?? "", summ: "\(task["price"] as? Int ?? 0)", date: "\(task["price"] as? Int ?? 0)", group_id: task["group_id"] as? Int ?? 0, user_id: task["user_id"] as? Int ?? 0, again: task["again"] as? String ?? "", remember: task["remember"] as? String ?? ""))
                                }
                            }
                            if dayColor == 0 {
                                if todayTasks.count == 0 {
                                    print("empty")
                                    tasksTable.isHidden = true
                                } else {
                                    totalTasks = todayTasks
                                    noTasksLabel.isHidden = true
                                    tasksTable.reloadData()
                                }
                                tasksTable.reloadData()
                            }
                            if dayColor == 1 {
                                if nextTasks.count == 0 {
                                    print("empty")
                                    tasksTable.isHidden = true
                                } else {
                                    totalTasks = nextTasks
                                    noTasksLabel.isHidden = true
                                    tasksTable.reloadData()
                                }
                                tasksTable.reloadData()
                            }
                            if dayColor == 2 {
                                if weekTasks.count == 0 {
                                    print("empty")
                                    tasksTable.isHidden = true
                                } else {
                                    totalTasks = weekTasks
                                    noTasksLabel.isHidden = true
                                    tasksTable.reloadData()
                                }
                                tasksTable.reloadData()
                            }

                            // try to read out a string array
                            //if let error = json["error"] as? String {
                            //    self.showAlertWithText(error)
                            //}
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                    }
                }
              semaphore.signal()
            }

            task.resume()
            //tasksTable.reloadData()
            semaphore.wait()
        } else {
            var semaphore = DispatchSemaphore (value: 0)

            let parameters = [
            ] as [[String : Any]]

            let boundary = "Boundary-\(UUID().uuidString)"
            var body = ""
            var error: Error? = nil
            for param in parameters {
              if param["disabled"] == nil {
                let paramName = param["key"]!
                body += "--\(boundary)\r\n"
                body += "Content-Disposition:form-data; name=\"\(paramName)\""
                if param["contentType"] != nil {
                  body += "\r\nContent-Type: \(param["contentType"] as! String)"
                }
                let paramType = param["type"] as! String
                if paramType == "text" {
                  let paramValue = param["value"] as! String
                  body += "\r\n\r\n\(paramValue)\r\n"
                } else {
                  let paramSrc = param["src"] as! String
                  let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                  let fileContent = String(data: fileData!, encoding: .utf8)!
                  body += "; filename=\"\(paramSrc)\"\r\n"
                    + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                }
              }
            }
            body += "--\(boundary)--\r\n";
            let postData = body.data(using: .utf8)
            
            let token = UserDefaults.standard.string(forKey: "token")
            let day = days[dayColor]
            //let day
            let urlString = "http://todo.teo-crm.com/api/task/index-group?token=\(token!)&groupId=\(vcID)"
            //&date=\(day)"
            print(urlString)
            var request = URLRequest(url: URL(string: urlString)!,timeoutInterval: Double.infinity)
            request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

            request.httpMethod = "GET"
            //request.httpBody = postData

            let task = URLSession.shared.dataTask(with: request) { data, response, error in
              guard let data = data else {
                print(String(describing: error))
                semaphore.signal()
                return
              }
                DispatchQueue.main.async { [self] () -> Void in
                    do {
                        //print("i am here")
                        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] {
                            print(json)
                            todayTasks.removeAll()
                            nextTasks.removeAll()
                            weekTasks.removeAll()
                            for task in json  {
                                if dayColor == 0 {
                                todayTasks.append(Task(id: String(task["id"] as? String ?? "0"), taskImage: task["avatar"] as? String ?? "", taskCreatorImage: task["photo_task"] as? String ?? "", name: task["name"] as? String ?? "", time: task["time"] as? String ?? "", summ: "\(task["price"] as? Int ?? 0)", date: "\(task["price"] as? Int ?? 0)", group_id: task["group_id"] as? Int ?? 0, user_id: task["user_id"] as? Int ?? 0, again: task["again"] as? String ?? "", remember: task["remember"] as? String ?? ""))
                                }
                                if dayColor == 1 {
                                nextTasks.append(Task(id: String(task["id"] as? String ?? "0"), taskImage: task["avatar"] as? String ?? "", taskCreatorImage: task["photo_task"] as? String ?? "", name: task["name"] as? String ?? "", time: task["time"] as? String ?? "", summ: "\(task["price"] as? Int ?? 0)", date: "\(task["price"] as? Int ?? 0)", group_id: task["group_id"] as? Int ?? 0, user_id: task["user_id"] as? Int ?? 0, again: task["again"] as? String ?? "", remember: task["remember"] as? String ?? ""))
                                }
                                if dayColor == 2 {
                                weekTasks.append(Task(id: String(task["id"] as? String ?? "0"), taskImage: task["avatar"] as? String ?? "", taskCreatorImage: task["photo_task"] as? String ?? "", name: task["name"] as? String ?? "", time: task["time"] as? String ?? "", summ: "\(task["price"] as? Int ?? 0)", date: "\(task["price"] as? Int ?? 0)", group_id: task["group_id"] as? Int ?? 0, user_id: task["user_id"] as? Int ?? 0, again: task["again"] as? String ?? "", remember: task["remember"] as? String ?? ""))
                                }
                            }
                            if dayColor == 0 {
                                if todayTasks.count == 0 {
                                    print("empty")
                                    tasksTable.isHidden = true
                                } else {
                                    totalTasks = todayTasks
                                    noTasksLabel.isHidden = true
                                    tasksTable.reloadData()
                                }
                                tasksTable.reloadData()
                            }
                            if dayColor == 1 {
                                if nextTasks.count == 0 {
                                    print("empty")
                                    tasksTable.isHidden = true
                                } else {
                                    totalTasks = nextTasks
                                    noTasksLabel.isHidden = true
                                    tasksTable.reloadData()
                                }
                                tasksTable.reloadData()
                            }
                            if dayColor == 2 {
                                if weekTasks.count == 0 {
                                    print("empty")
                                    tasksTable.isHidden = true
                                } else {
                                    totalTasks = weekTasks
                                    noTasksLabel.isHidden = true
                                    tasksTable.reloadData()
                                }
                                tasksTable.reloadData()
                            }

                            // try to read out a string array
                            //if let error = json["error"] as? String {
                            //    self.showAlertWithText(error)
                            //}
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                    }
                }
              semaphore.signal()
            }

            task.resume()
            //tasksTable.reloadData()
            semaphore.wait()
        }
    }
    
    override func viewDidLoad() {
        getInfo()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        super.viewDidLoad()
        getUserID()
        daysCollection.delegate = self
        daysCollection.dataSource = self
//        if todayTasks.count == 0 {
//            totalTasks = todayTasks
//            tasksTable.isHidden = true
//        } else {
//            noTasksLabel.isHidden = true
//        }
        let productCell = UINib(nibName: "DaysCells",
                                  bundle: nil)
        self.daysCollection.register(productCell,
                                     forCellWithReuseIdentifier: "DaysCells")
        let taskCell = UINib(nibName: "TaskCell",
                                  bundle: nil)
        self.tasksTable.register(taskCell,
                                 forCellReuseIdentifier: "TaskCell")
        tasksTable.delegate = self
        tasksTable.dataSource = self
        tasksTable.tableFooterView = UIView(frame: .zero)
    }
    
    @IBAction func slideTapped(_ sender: Any) {
    }
    
    @IBAction func calendarTapped(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "CalendarTasksViewController") as! CalendarTasksViewController
        vc.totalTasks = totalTasks
        vc.vcID = vcID
        vc.groupName = groupName
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func settingTapped(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "SettingsViewController") as! SettingsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    var groupName = ""
    
    @IBAction func addTask(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "NewTaskViewController") as! NewTaskViewController
        vc.delegate = self
        vc.groupID = vcID
        vc.groupName = groupName
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = storyboard?.instantiateViewController(identifier: "NewTaskViewController") as! NewTaskViewController
        vc.delegate = self
        vc.vcID = 1
        vc.idEdit = totalTasks[indexPath.row].id
        vc.nameEdit = totalTasks[indexPath.row].name
        vc.summEdit = totalTasks[indexPath.row].summ
        vc.certainTimeEdit = totalTasks[indexPath.row].time
        vc.dateEdit = totalTasks[indexPath.row].date
        vc.remindEdit = totalTasks[indexPath.row].remember
        vc.repeatEdit = totalTasks[indexPath.row].again
        vc.idMemberToSend = String(totalTasks[indexPath.row].user_id)
        vc.idGroupToSend = String(totalTasks[indexPath.row].group_id)
        if let url = URL(string: "http://todo.teo-crm.com/\(totalTasks[indexPath.row].taskImage)"){
            
            if let cachedImage = self.model.imageCache.object(forKey: url.absoluteString as NSString) {
                vc.imageEdit = cachedImage
            } else {
                do {
                    let data = try Data(contentsOf: url)
                    vc.imageEdit = UIImage(data: data)
                    self.model.imageCache.setObject((UIImage(data: data) ?? UIImage(named: "user"))!, forKey: url.absoluteString as NSString)
                } catch let err {
                    print("Error: \(err.localizedDescription)")
                }
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getUserID(){
        var semaphore = DispatchSemaphore (value: 0)

        let parameters = [
        ] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)
        
        let token = UserDefaults.standard.string(forKey: "token")
        let urlString = "http://todo.teo-crm.com/api/user/info?token=\(token!)"

        var request = URLRequest(url: URL(string: urlString)!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "GET"
        //request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            semaphore.signal()
            return
          }
            DispatchQueue.main.async { [self] () -> Void in
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        UserDefaults.standard.setValue(json["id"], forKey: "id")
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
            semaphore.signal()
        }

        task.resume()
        semaphore.wait()
    }
}
