//
//  SettingsViewController.swift
//  ToDo
//
//  Created by Святослав Шевченко on 17.02.2021.
//

import UIKit
import Alamofire
import MessageUI

class SettingsViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, MFMailComposeViewControllerDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return 24
        } else {
            return 60
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if row < 10 {
            return "0\(row)"
        } else {
            return "\(row)"
        }
    }
    
    var hours = ""
    var minutes = ""
    
    var pickerTime = ""
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 {
            if row < 10 {
            hours = "0\(row)"
            } else {
                hours = "\(row)"
            }
        } else {
            if row < 10 {
                minutes = "0\(row)"
            } else {
                minutes = "\(row)"
            }
        }
        pickerTime = hours + ":" + minutes
    }
    
    
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var tickButton: UIButton!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var timePicker: UIPickerView!
    @IBOutlet weak var selectTimeView: UIView!
    @IBOutlet weak var fioTF: UITextField!
    
    
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var bigNameLabel: UILabel!
    
    
    var imagePicker = UIImagePickerController()
    
    let model = DataModel.shared
    
    override func viewWillAppear(_ animated: Bool) {
        var semaphore = DispatchSemaphore (value: 0)

        let parameters = [
        ] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)
        
        let token = UserDefaults.standard.string(forKey: "token")
        let urlString = "http://todo.teo-crm.com/api/user/info?token=\(token!)"

        var request = URLRequest(url: URL(string: urlString)!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "GET"
        //request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            semaphore.signal()
            return
          }
            DispatchQueue.main.async { [self] in
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        print(json)
                        UserDefaults.standard.setValue(json["id"], forKey: "id")
                        phoneLabel.text = json["phone"] as? String ?? ""
                        fioTF.text = json["fio"] as? String ?? "Введите ваше имя"
                        DispatchQueue.main.async { [self] in
                            if let url = URL(string: "http://todo.teo-crm.com/\(json["avatar"] as? String ?? "")"){
                                if let cachedImage = self.model.imageCache.object(forKey: url.absoluteString as NSString) {
                                    profileImage.image = cachedImage
                                } else {
                                    DispatchQueue.global(qos: .userInitiated).async {
                                        let imageData = NSData.init(contentsOf: url)
                                        DispatchQueue.main.async {
                                            profileImage.image = UIImage(data: imageData! as Data)
                                            self.model.imageCache.setObject((UIImage(data: imageData! as Data) ?? UIImage(named: "user"))!, forKey: url.absoluteString as NSString)
                                        }
                                    }
                                }
                            } else {
                                profileImage.image = UIImage(named: "user")
                            }
                        }
                        if json["notification_enable"] as? Bool ?? false {
                            tickButton.setImage(UIImage(named: "checkboxon"), for: .normal)
                        } else {
                            tickButton.setImage(UIImage(named: "checkboxoff"), for: .normal)
                        }
                        timeLabel.text = json["notification_time"] as? String ?? "09:41"
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
            semaphore.signal()
        }

        task.resume()
        semaphore.wait()
    }
    
    @IBOutlet weak var miniPickerView: UIView!
    
    
    override func viewDidLoad() {
        miniPickerView.layer.cornerRadius = 15
        fioTF.delegate = self
        super.viewDidLoad()
        getDataForSupport()
        timeView.layer.cornerRadius = 6
        profileImage.layer.cornerRadius = 21
        selectTimeView.isHidden = true
        timePicker.delegate = self
        timePicker.dataSource = self
        timePicker.layer.cornerRadius = 20
    }
    
    @IBAction func timeTapped(_ sender: Any) {
        selectTimeView.isHidden = false
    }
    
    @IBAction func doneTapped(_ sender: Any) {
        timeLabel.text = pickerTime
        selectTimeView.isHidden = true
    }
    
    @IBAction func backTimeTapped(_ sender: Any) {
        selectTimeView.isHidden = true
    }
    
    
    @IBAction func slideLeftTapped(_ sender: Any) {
        var notification_enable = "1"
        if tickButton.imageView?.image == UIImage(systemName: "square") {
            notification_enable = "0"
        }
        
        var nameFIO = fioTF.text ?? ""
        
//        if fioTF.text == nil || fioTF.text == "" {
//            nameFIO = bigNameLabel.text ?? ""
//        } else {
//            nameFIO = fioTF.text ?? ""
//        }
        
        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token"),
            "type": "text"
          ],
          [
            "key": "fio",
            "value": nameFIO,
            "type": "text"
          ],
          [
            "key": "notification_enable",
            "value": notification_enable,
            "type": "text"
          ],
          [
            "key": "notification_time",
            "value": timeLabel.text,
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "http://todo.teo-crm.com/api/user/edit")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()
    }
    
    
    @IBAction func tickTapped(_ sender: Any) {
        if tickButton.imageView?.image == UIImage(named: "checkboxoff") {
            tickButton.setImage(UIImage(named: "checkboxon"), for: .normal)
        } else {
            tickButton.setImage(UIImage(named: "checkboxoff"), for: .normal)
        }
    }
    
    @IBAction func photoTapped(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){

                    imagePicker.delegate = self
                    imagePicker.sourceType = .savedPhotosAlbum
                    imagePicker.allowsEditing = false

                    present(imagePicker, animated: true, completion: nil)
                }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        profileImage.image = image
        let image_data = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)!
        let imageData:Data = image_data.pngData()!
        let imageStr = imageData.base64EncodedString()
        //let imageURL = info[UIImagePickerController.InfoKey.referenceURL] as! NSURL
        //let imageName = imageURL.lastPathComponent
        //let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! as String
        //let localPath = documentDirectory.stringByAppendingPathComponent(imageName)
        //postImage(path: imageStr)
        imageUploadRequest(imageView: image, uploadUrl: URL(string: "http://todo.teo-crm.com/api/user/upload-photo")! as NSURL, param: ["token" : UserDefaults.standard.string(forKey: "token")!])
    }
    
    func postImage(path: String){
        
        
        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token"),
            "type": "text"
          ],
          [
            "key": "avatar",
            "src": path,
            "type": "file"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
                guard let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data else { return  }
              let fileContent = String(data: fileData, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "http://todo.teo-crm.com/api/user/upload-photo")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()
    }
    
    // SEND IMAGE
    
    func imageUploadRequest(imageView: UIImage, uploadUrl: NSURL, param: [String:String]?) {

            //let myUrl = NSURL(string: "http://192.168.1.103/upload.photo/index.php");

        let request = NSMutableURLRequest(url:uploadUrl as URL);
        request.httpMethod = "POST"

            let boundary = generateBoundaryString()

            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        let imageData = imageView.jpegData(compressionQuality: 1)

            if(imageData==nil)  { return; }

        request.httpBody = createBodyWithParameters(parameters: param, filePathKey: "avatar", imageDataKey: imageData! as NSData, boundary: boundary) as Data

            //myActivityIndicator.startAnimating();

        let task =  URLSession.shared.dataTask(with: request as URLRequest,
                completionHandler: {
                    (data, response, error) -> Void in
                    if let data = data {

                        // You can print out response object
                        print("******* response = \(response)")

                        print(data.count)
                        // you can use data here

                        // Print out reponse body
                        let responseString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                        print("****** response data = \(responseString!)")

                        let json =  try!JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary

                        print("json value \(json)")

                        //var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers, error: &err)

                    } else if let error = error {
                        print(error)
                    }
            })
            task.resume()


        }


        func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
            let body = NSMutableData();

            if parameters != nil {
                for (key, value) in parameters! {
                    body.appendString(string: "--\(boundary)\r\n")
                    body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                    body.appendString(string: "\(value)\r\n")
                }
            }

            let filename = "user-profile.jpg"

            let mimetype = "image/jpg"

            body.appendString(string: "--\(boundary)\r\n")
            body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
            body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
            body.append(imageDataKey as Data)
            body.appendString(string: "\r\n")

            body.appendString(string: "--\(boundary)--\r\n")

            return body
        }

        func generateBoundaryString() -> String {
            return "Boundary-\(NSUUID().uuidString)"
        }
    
    @IBAction func supportTapped(_ sender: Any) {
        print("send this")
        if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.setToRecipients([emailToSend])

                present(mail, animated: true)
            } else {
            }
    }
    
    @IBAction func shareTapped(_ sender: Any) {
        let vc = UIActivityViewController(activityItems: [textToSend], applicationActivities: [])
        present(vc, animated: true)
        
        print(textToSend)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case .cancelled:
            controller.dismiss(animated: true, completion: nil)
        case .failed:
            controller.dismiss(animated: true, completion: nil)
        case .saved:
            controller.dismiss(animated: true, completion: nil)
        case .sent:
            controller.dismiss(animated: true, completion: nil)
        @unknown default:
            print("default")
        }
    }
    
    var emailToSend = "test@mail.ru"
    var textToSend = "Try the app!"
    
    func getDataForSupport(){
        let parameters = [
        ] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)
        
        
        let token = UserDefaults.standard.string(forKey: "token")
        let urlString = "http://todo.teo-crm.com/api/user/help?token=\(token!)"

        var request = URLRequest(url: URL(string: urlString)!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
            DispatchQueue.main.async { [self] in
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        emailToSend = json["email"] as? String ?? "test@mail.ru"
                        textToSend = json["text"] as? String ?? "Try the app!"
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
        }
        
        task.resume()
    }
}

extension NSMutableData {

    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
